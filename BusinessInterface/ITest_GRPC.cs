﻿using MagicOnion;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lp.SheepInterface
{
    public interface ITest_GRPC : IService<ITest_GRPC>
    {
        /// <summary>
        /// 测试
        /// </summary>
        /// <returns></returns>
        UnaryResult<int> Test();
    }
}
