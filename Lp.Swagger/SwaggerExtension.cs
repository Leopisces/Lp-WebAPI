﻿using Lp.Core.Attributes;
using Lp.Core.Entity;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerUI;
using System.Collections.Generic;
using System.IO;

namespace Lp.Swagger
{
    /// <summary>
    /// 服务集合扩展
    /// </summary>
    public static class SwaggerExtension
    {
        /// <summary>
        /// 注入自定义的swagger服务
        /// </summary>
        /// <param name="services"></param>
        /// <param name="apiInfo"></param>
        /// <returns></returns>
        public static IServiceCollection AddCustomSwagger(this IServiceCollection services, IApiInfo apiInfo)
        {
            services.AddSwaggerGen(options =>
            {
                // 自定义OperationId
                options.CustomOperationIds(e => $"{e.ActionDescriptor.RouteValues["controller"]}_{e.HttpMethod}");
                options.SwaggerDoc(apiInfo.ApiName, new OpenApiInfo
                {
                    Title = apiInfo.Title,
                    Version = apiInfo.Version,
                    Description = apiInfo.Version
                });
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlpath = Path.Combine(basePath, apiInfo.ApplicationAssembly.GetName().Name + ".xml");
                if (File.Exists(xmlpath))
                {
                    options.IncludeXmlComments(xmlpath);
                }
                //TOKEN
                ///添加认证定义
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "在下框中输入请求头中需要添加Jwt授权Token：Bearer Token",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Scheme = "Bearer",
                    Type = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT"
                });
                ///添加安全要求
                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new List<string>()
                    }
                });
                options.DocumentFilter<HiddenFilter>();
            });
            return services;
        }
        public static IApplicationBuilder UseCustomSwagger(this IApplicationBuilder app, IApiInfo apiInfo)
        {
            app.UseSwagger(c =>
            {
                c.RouteTemplate = "/{documentName}/swagger.json";
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/{apiInfo.ApiName}/swagger.json", $"{apiInfo.Title} {apiInfo.ApiName} {apiInfo.Version}");
                //SWAGGER UI折叠
                c.DocExpansion(DocExpansion.None);
                //不显示model
                c.DefaultModelExpandDepth(-1);
            });
            return app;
        }
    }
}
