﻿using Consul;
using Grpc.Core;
using MagicOnion.Server;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Lp.Consul;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Hosting;
using Lp.Consul.Registry;

namespace Lp.Grpc
{
    /// <summary>
    /// 管道扩展
    /// </summary>
    public static class GrpcExtension
    {
        /// <summary>
        /// 注入
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddGrpcClient(this IServiceCollection services)
        {
            services.AddSingleton<IGRpcConnection, GRpcConnection>();
            services.AddSingleton<IGrpcChannelFactory, GrpcChannelFactory>();
            return services;
        }

        /// <summary>
        ///通过地址注册服务代理,当应用程序停止时，这些服务代理将注销
        /// </summary>
        /// <param name="app"></param>
        /// <param name="configuration"></param>
        /// <param name="apiInfo">api信息</param>
        /// <returns></returns>
        public static IApplicationBuilder UseGrpcConsulRegisterService(this IApplicationBuilder app, IConfiguration configuration)
        {
            ConsulServiceDiscoveryOption serviceDiscoveryOption = new ConsulServiceDiscoveryOption();
            //获取配置文件里面的服务注册地址
            configuration.GetSection("ServiceDiscovery").Bind(serviceDiscoveryOption);
            serviceDiscoveryOption.ServiceName = serviceDiscoveryOption.ServiceName;
            app.UseGrpcConsulRegisterService(serviceDiscoveryOption, int.Parse(configuration.GetSection("Ports:rpc").Value));
            return app;
        }

        /// <summary>
        /// 通过地址注册服务代理,当应用程序停止时，这些服务代理将注销
        /// 如果服务不存在则GetService()返回null，GetRequiredService()而是抛出异常。
        /// 如果您正在使用第三方容器，请尽可能使用GetRequiredService- 如果发生异常，
        /// 第三方容器可能就会根据异常信息提供相应的诊断信息，以便您可以找出未注册预期服务的原因。
        /// IApplicationLifetime:应用启动和关闭时对相关组件发送相应的信号或者通知
        /// </summary>
        /// <param name="app"></param>
        /// <param name="serviceDiscoveryOption">服务注册的配置实体</param>
        /// <param name="apiInfo">api信息</param>
        /// <returns></returns>
        public static IApplicationBuilder UseGrpcConsulRegisterService(this IApplicationBuilder app, ConsulServiceDiscoveryOption serviceDiscoveryOption, int port)
        {
            var applicationLifetime = app.ApplicationServices.GetRequiredService<IHostApplicationLifetime>() ??
                 throw new ArgumentException("缺少依赖项", nameof(IHostApplicationLifetime));

            if (serviceDiscoveryOption.Consul == null)
                throw new ArgumentException("缺少依赖项", nameof(serviceDiscoveryOption.Consul));
            var consul = app.ApplicationServices.GetRequiredService<IConsulClient>() ?? throw new ArgumentException("缺少依赖项", nameof(IConsulClient));

            //create logger to record the important information
            var loggerFactory = app.ApplicationServices.GetRequiredService<ILoggerFactory>();
            var logger = loggerFactory.CreateLogger("MisServiceBuilder");

            if (string.IsNullOrEmpty(serviceDiscoveryOption.ServiceName))
                throw new ArgumentException("请配置服务名称", nameof(serviceDiscoveryOption.ServiceName));
            IEnumerable<Uri> addresses = null;
            if (serviceDiscoveryOption.Endpoints != null && serviceDiscoveryOption.Endpoints.Length > 0)
            {
                logger.LogInformation($"用 {serviceDiscoveryOption.Endpoints.Length} 为服务注册配置的终结点");
                addresses = serviceDiscoveryOption.Endpoints.Select(p => new Uri(p));
            }
            else
            {
                logger.LogInformation($"Trying to use server.Features to figure out the service endpoint for registration.");
                var features = app.Properties["server.Features"] as FeatureCollection;
                addresses = features.Get<IServerAddressesFeature>().Addresses.Select(p => new Uri(p)).ToArray();
                logger.LogInformation(@"addresses:" + addresses.FirstOrDefault() + ",count:" + addresses.Count());
            }
            logger.LogInformation("GRPC服务的端口:" + port);
            var grpcServer = InitializeGrpcServer(port);

            foreach (var address in addresses)
            {
                logger.LogInformation("address:" + address.Host + "," + address.Port + "," + address);
                UriBuilder myUri = new UriBuilder(address.Scheme, address.Host, port);
                ///获取服务编号
                var serviceID = GetServiceId(serviceDiscoveryOption.ServiceName, myUri.Uri);

                logger.LogInformation($"通过: {address},来注册服务编号: {serviceID}.");
                Uri healthCheck = null;
                if (!string.IsNullOrEmpty(serviceDiscoveryOption.HealthCheckTemplate))
                {
                    healthCheck = new Uri(address, serviceDiscoveryOption.HealthCheckTemplate);

                    logger.LogInformation($"Adding healthcheck for {serviceID},checking {healthCheck}");
                }
                ///添加租户
                var registryInformation = app.AddTenant(serviceDiscoveryOption.ServiceName, serviceDiscoveryOption.Version, myUri.Uri, healthCheckUri: healthCheck, tags: new[] { $"urlprefix-/{serviceDiscoveryOption.ServiceName}" });
                logger.LogInformation("注册附加健康检查");
                // 注册服务和运行状况检查清理
                applicationLifetime.ApplicationStopping.Register(() =>
                {
                    try
                    {
                            //关闭异步
                            grpcServer.ShutdownAsync().Wait();
                    }
                    catch (Exception ex)
                    {
                        logger.LogError($"grpc服务关闭: {ex}");
                    }
                    logger.LogInformation("删除租户和附加运行状况检查");
                        ///移除租户
                        app.RemoveTenant(registryInformation.Id);
                });
            }
            return app;
        }
        /// <summary>
        /// 根据服务名和url获取服务id
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="uri"></param>
        /// <returns></returns>
        private static string GetServiceId(string serviceName, Uri uri)
        {
            return $"{serviceName}_{uri.Host.Replace(".", "_")}_{uri.Port}";
        }

        /// <summary>
        /// 根据api信息初始化服务,启动服务
        /// </summary>
        /// <param name="apiInfo"></param>
        /// <returns></returns>
        private static Server InitializeGrpcServer(int BindPort)
        {

            var service = MagicOnionEngine.BuildServerServiceDefinition(true
            ////客户端提示:One or more errors occurred. (Status(StatusCode=Unavailable, Detail="Connect Failed"))
            ////new[] { typeof(Test).Assembly },  // 加载引用程序集
            //new MagicOnionOptions(true){
            //  MagicOnionLogger = new MagicOnionLogToGrpcLogger(),
            //}
            );

            var grpcServer = new Server
            {
                Ports = { new ServerPort("0.0.0.0", BindPort, ServerCredentials.Insecure) },
                Services = { service }
            };
            //先注释
            grpcServer.Start();
            return grpcServer;

        }

        /// <summary>
        /// 添加服务租户
        /// </summary>
        /// <param name="app"></param>
        /// <param name="serviceName"></param>
        /// <param name="version"></param>
        /// <param name="uri"></param>
        /// <param name="healthCheckUri"></param>
        /// <param name="tags"></param>
        /// <returns></returns>
        public static RegistryInformation AddTenant(this IApplicationBuilder app, string serviceName, string version, Uri uri, Uri healthCheckUri = null, IEnumerable<string> tags = null)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            var serviceRegistry = app.ApplicationServices.GetRequiredService<ServiceRegistry>();
            var registryInformation = serviceRegistry.RegisterServiceAsync(serviceName, version, uri, healthCheckUri, tags)
                .Result;

            return registryInformation;
        }

        /// <summary>
        /// 移除服务租户
        /// </summary>
        /// <param name="app"></param>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        public static bool RemoveTenant(this IApplicationBuilder app, string serviceId)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }
            if (string.IsNullOrEmpty(serviceId))
            {
                throw new ArgumentNullException(nameof(serviceId));
            }

            var serviceRegistry = app.ApplicationServices.GetRequiredService<ServiceRegistry>();
            return serviceRegistry.DeregisterServiceAsync(serviceId)
                .Result;
        }


    }

}
