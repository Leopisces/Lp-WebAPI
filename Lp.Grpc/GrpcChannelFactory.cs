﻿using Grpc.Core;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace Lp.Grpc
{
    public class GrpcChannelFactory : IGrpcChannelFactory
    {
        private static readonly object _syncLock = new object();
        private Dictionary<string, Channel> grpcServers;
        private IConfiguration _configuration;

        public GrpcChannelFactory(IConfiguration configuration)
        {
            grpcServers = new Dictionary<string, Channel>();

            _configuration = configuration;
        }
        /// <summary>
        /// 根据ip,port获取Channel
        /// </summary>
        /// <param name="address"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        public Channel Get(string address, int port)
        {
            try
            {
                Channel channel = null;
                int MAXVALUE = 10 * 1024 * 1024;
                var options = new[]
                { 
                    // 此channel参数控制在transport上发送keepalive ping的时间间隔（以毫秒为单位）
                    new ChannelOption("grpc.keepalive_time_ms", 10000),
                    // 此channel参数控制keepalive ping的发送方等待确认的时间（以毫秒为单位）。如果在此时间内未收到确认，它将关闭连接。
                    new ChannelOption("grpc.keepalive_timeout_ms", 5000),
                    // 每10秒允许来自客户端的grpc ping
                    new ChannelOption("grpc.http2.min_time_between_pings_ms", 10000),
                    // 当没有其他数据（数据帧或标头帧）要发送时，此通道参数控制可发送的最大ping数。如果超出限制，GRPC Core将不会继续发送ping。将其设置为0将允许在不发送数据的情况下发送ping命令。
                    new ChannelOption("grpc.http2.max_pings_without_data", 0),
                    // 如果将此通道参数设置为1（0：false; 1：true），则即使没有请求进行，也可以发送keepalive ping
                    new ChannelOption("grpc.keepalive_permit_without_calls", 1),
                    // 每5秒钟允许一次来自客户端的grpc ping而无数据
                    new ChannelOption("grpc.http2.min_ping_interval_without_data_ms", 5000),
                    //设置接收的大小
                    new ChannelOption("grpc.max_receive_message_length", MAXVALUE),
                    ////设置发送的大小
                    new ChannelOption("grpc.max_send_message_length", MAXVALUE)
                };

                string key = $"{address}:{port}";
                if (!grpcServers.TryGetValue(key, out channel))
                {
                    lock (_syncLock)
                    {
                        if (_configuration.GetSection("ChannelCheck").Value == "true")
                            channel = new Channel(address, port, ChannelCredentials.Insecure, options);
                        else
                            channel = new Channel(address, port, ChannelCredentials.Insecure);
                        if (!grpcServers.ContainsKey(key))
                            grpcServers.Add(key, channel);
                    }
                }
                return channel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
