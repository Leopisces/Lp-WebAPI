﻿using Consul;
using MagicOnion;
using MagicOnion.Client;
using Lp.LogAnalysis;
using Lp.Consul;
using System;
using System.Threading.Tasks;

namespace Lp.Grpc
{
    public class GRpcConnection : IGRpcConnection
    {
        /// <summary>
        /// 服务用户
        /// </summary>
        private IGrpcChannelFactory _grpcChannelFactory;
        //private ILogClient logClient;
        private readonly IConsulClient _consulClient;


        public GRpcConnection(IGrpcChannelFactory grpcChannelFactory
            //, ILogClient logClient
            , IConsulClient consulClient)
        {
            this._grpcChannelFactory = grpcChannelFactory;
            //this.logClient = logClient;
            this._consulClient = consulClient;
        }
        /// <summary>
        /// 获取服务
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <param name="serviceName"></param>
        /// <returns></returns>
        public async Task<TService> GetRemoteService<TService>(string serviceName) where TService : IService<TService>
        {
            try
            {
                AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

                var servicesTask = await _consulClient.Health.Service(serviceName).ConfigureAwait(false);
                ///创建个Channel
                var serviceChannel = _grpcChannelFactory.Get(servicesTask.Response[0].ToEndpoint().Address, servicesTask.Response[0].ToEndpoint().Port);

                return MagicOnionClient.Create<TService>(serviceChannel);
            }
            catch (Exception ex)
            {
                //logClient.WriteError("获取grpc服务出错", ex);
                return default(TService);
            }
        }
    }
}
