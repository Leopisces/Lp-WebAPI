﻿using Grpc.Core;

namespace Lp.Grpc
{
    /// <summary>
    /// GRpc <see cref="Channel"/> Channel Factory
    /// </summary>
    public interface IGrpcChannelFactory
    {
        /// <summary>
        /// 基于提供的GRPC服务器地址创建个Channel渠道
        /// </summary>
        /// <param name="address">GRpc server address</param>
        /// <param name="port">GRpc server port</param>
        /// <returns></returns>
        Channel Get(string address, int port);
    }
}
