﻿using MagicOnion;
using System.Threading.Tasks;

namespace Lp.Grpc
{
    public interface IGRpcConnection
    {
        /// <summary>
        /// 获取指定的远程服务接口
        /// </summary>
        ///<typeparam name="TService">远程服务接口类型</typeparam>
        ///<param name = "serviceName"> 远程服务名称</param>
        Task<TService> GetRemoteService<TService>(string serviceName) where TService : IService<TService>;
    }
}
