using Lp.Core.DI;
using Lp.JWT;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using Lp.Core.Entity;
using Lp.ServiceExtension;
using Authentication.Controllers;

namespace Authentication
{
    /// <summary>
    /// Startup
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 构造
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// IConfiguration
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            //通用注册
            services.AddCommonServer(Configuration, GetType().Assembly);

            //注入数据层
            foreach (var item in CommonMethod.InjectClassTypes("Authentication.Dal", "I", "_Imp"))
            {
                services.AddTransient(item.Value, item.Key);
            }
            
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="apiInfo"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiInfo apiInfo)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCommonService(env, Configuration, apiInfo);
            DIContainer.ServiceLocator.Instance = app.ApplicationServices;
        }
    }
}
