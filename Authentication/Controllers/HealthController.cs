﻿
using Microsoft.AspNetCore.Mvc;

namespace Authentication.Controllers
{
    /// <summary>
    /// 健康检查
    /// </summary>
    [Produces("application/json")]
    [Route("/api/Authentication/Health")]
    public class HealthController : Controller
    {
        /// <summary>
        /// OK
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get() => Ok("ok");
    }
}