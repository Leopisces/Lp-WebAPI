﻿using Authentication.Dal;
using Lp.Core.Attributes;
using Lp.Core.Entity;
using Lp.Core.Enum;
using Lp.Core.Extensions;
using Lp.Core.Helper;
using Lp.JWT;
using Lp.LogAnalysis;
using Lp.Redis;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Authentication
{
    /// <summary>
    /// 登录,生成token
    /// </summary>
    //[HiddenApi]
    [Produces("application/json")]
    public class PermissionController : Controller
    {
        private IRedisContext _redisContext;
        private ILogClient _logClient;
        private readonly IHttpContextAccessor _contextAccessor;
        private IConfiguration _configuration;
        private readonly IBas_User_Imp _user_Imp;

        /// <summary>
        /// 自定义策略参数
        /// </summary>
        PermissionRequirement _requirement;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="requirement"></param>
        /// <param name="redisContext"></param>
        /// <param name="logClient"></param>
        /// <param name="contextAccessor"></param>
        /// <param name="configuration"></param>
        /// <param name="user_Imp"></param>
        public PermissionController(PermissionRequirement requirement
            , IRedisContext redisContext
        //, ILogClient logClient
        , IHttpContextAccessor contextAccessor
        , IConfiguration configuration
        , IBas_User_Imp user_Imp
        )
        {
            _requirement = requirement;
            _redisContext = redisContext;
            //_logClient = logClient;
            _contextAccessor = contextAccessor;
            _configuration = configuration;
            _user_Imp = user_Imp;
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="account">账号</param>
        /// <param name="password">密码</param>
        /// <param name="Platform">平台(web,android,ios,other,h5)</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("/api/Login")]
        public async Task<ResponseResult<LoginInfo>> Login([Required] string account, [Required] string password, [Required] string Platform)
        {
            var result = ResponseResult<LoginInfo>.Default();
            //获取人员信息
            ClientInformation userinfo = new ClientInformation();

            if (StringExtensions.IsNullOrWhitespace(account))
            {
                return ResponseResult<LoginInfo>.Error(MessageHelper.UserNameCannotBeEmpty);
            }

            if (StringExtensions.IsNullOrWhitespace(password))
            {
                return ResponseResult<LoginInfo>.Error(MessageHelper.PasswordCannotBeEmpty);
            }

            if (StringExtensions.IsNullOrWhitespace(Platform))
            {
                return ResponseResult<LoginInfo>.Error(MessageHelper.PlatformCannotBeEmpty);
            }

            if (Platform != "web" && Platform != "android" && Platform != "ios" && Platform != "other" && Platform != "h5")
            {
                return ResponseResult<LoginInfo>.Error(MessageHelper.PlatformCode);
            }

            try
            {
                password = EncryptHelper.AESEncrypt(password);
                var users = await _user_Imp.List(it => it.Account == account && it.Password == password);
                if (users == null || users.Count == 0)
                    return ResponseResult<LoginInfo>.Error("用户名或密码错误!");
                if (users.Count > 1)
                    return ResponseResult<LoginInfo>.Error("系统错误，用户重复!");
                if (users.Count == 1)
                {
                    userinfo = users[0];
                }
                userinfo.PlatForm = Platform;

                //如果是基于用户的授权策略，这里要添加用户;如果是基于角色的授权策略，这里要添加角色
                IPAddress ip = _contextAccessor.HttpContext.Connection.LocalIpAddress;
                if (ip == null)
                {
                    ip = IPAddress.Parse("127.0.0.1");
                }
                Claim[] claims = new Claim[] {
                    new Claim(ClaimTypes.Name, account),
                    new Claim(ClaimTypes.Role, "admin"),
                    new Claim(ClaimTypes.Expiration,  DateTime.Now.AddSeconds(_requirement.Expiration.TotalSeconds).ToString("yyyy-MM-dd HH:mm:ss")),
                    new Claim("PlatForm", Platform) ,
                    new Claim("UserID", userinfo.UserID.ToString()),
                    new Claim("IP", ip.MapToIPv4().ToString()) ,
                };
                //用户标识
                var identity = new ClaimsIdentity(JwtBearerDefaults.AuthenticationScheme);
                identity.AddClaims(claims);

                dynamic token = JwtToken.BuildJwtToken(claims, _requirement);
                LoginInfo login = JsonConvert.DeserializeObject<LoginInfo>(JsonConvert.SerializeObject(token));
                login.BasicInfo = userinfo;
                login.expires_in /= 1000;
                result = ResponseResult<LoginInfo>.Success(login, MessageHelper.EMP_LOGIN + MessageHelper.SUCCESS);
                _redisContext.Set(
                    string.Format(RedisConfigure.LOGIN.RedisKey, login.BasicInfo.Account, login.BasicInfo.RealName)
                    , login
                    , TimeSpan.FromSeconds(_requirement.Expiration.TotalSeconds)
                );
            }
            catch (Exception e)
            {
                result = ResponseResult<LoginInfo>.Error(e.Message);
                //_logClient.WriteError("登录写入redis", e);
            }

            return result;
        }


        /// <summary>
        /// 拒绝访问
        /// </summary>
        /// <returns></returns>
        [HttpGet("/api/Denied")]
        public IActionResult Denied()
        {
            return new JsonResult(new
            {
                Status = false,
                Message = "你无权限访问"
            });
        }
    }
}
