﻿using Authentication.Model;
using Lp.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Authentication.Dal
{
    public interface IBas_User_Imp
    {
        Task<List<ClientInformation>> List(Expression<Func<Bas_User, bool>> expression);
    }
}
