﻿using Authentication.Model;
using Lp.Core.Entity;
using Lp.SqlSugarORM;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Authentication.Dal
{
    /// <summary>
    /// 用户表数据层
    /// </summary>
    public class Bas_User_Imp : BaseDataRepository<Bas_User>, IBas_User_Imp
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="options"></param>
        public Bas_User_Imp(IOptions<SugarOption> options) : base(options)
        {

        }

        /// <summary>
        /// 初始化数据库连接
        /// </summary>
        protected override void init()
        {
            DatabaseEnum = SqlDatabaseEnum.PRIMARY;
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public new async Task<List<ClientInformation>> List(Expression<Func<Bas_User, bool>> expression)
        {
            return await Task.Run(() =>
            {
                var db = Db;
                var info = db.Queryable<Bas_User>().Where(expression).Select(it => new ClientInformation
                {
                    UserID = it.User_ID,
                    Code = it.Code,
                    RealName = it.Name,
                    Nick_Name = it.Nick_Name,
                    Account = it.Account
                }).ToList();
                return info;
            });

        }
    }
}
