﻿using Lp.Grpc;
using Lp.ServiceExtension;
using Lp.SheepInterface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sheep_API.Controllers
{
    /// <summary>
    /// 测试
    /// </summary>
    [Route("api/Sheep_API/[controller]/[action]")]
    [ApiController]
    [Produces("application/json")]
    [Authorize("Permission")]
    public class TestController : Controller
    {
        private readonly IGRpcConnection _gRpcConnection;

        /// <summary>
        /// 测试
        /// </summary>
        /// <param name="gRpcConnection"></param>
        public TestController(IGRpcConnection gRpcConnection)
        {
            _gRpcConnection = gRpcConnection;
        }

        /// <summary>
        /// Test 分支测试最终
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<int> Test()
        {
            var r = await _gRpcConnection.Get_ITest_GRPC().Test();
            return r;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult ApolloTest([FromServices] IConfiguration configuration, string key)
        {
            return Content(configuration.GetValue<string>(key));
        }
    }
}
