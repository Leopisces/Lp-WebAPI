﻿
using Microsoft.AspNetCore.Mvc;

namespace Sheep_API.Controllers
{
    /// <summary>
    /// 健康检查
    /// </summary>
    [Produces("application/json")]
    [Route("/api/Sheep_API/Health")]
    public class HealthController : Controller
    {
        /// <summary>
        /// OK
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get() => Ok("ok");
    }
}