﻿using System.Collections.Generic;

namespace Lp.LogAnalysis
{
    /// <summary>
    /// 业务日志日志实体
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MogoMessage<T>
    {
        /// <summary>
        /// 编码
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// IP
        /// </summary>
        public string IP { get; set; }
        /// <summary>
        /// 机器名
        /// </summary>
        public string MachineName { get; set; }
        /// <summary>
        /// 服务名
        /// </summary>
        public string DataBase { get; set; }
        /// <summary>
        /// 标签
        /// </summary>
        public string Tag { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateOper { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }
        /// <summary>
        /// 扩展内容
        /// </summary>
        public object Extra { get; set; }
        /// <summary>
        /// 消息
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 列表数据
        /// </summary>
        public List<T> Data { get; set; }
        /// <summary>
        /// 工单号
        /// </summary>
        public int WorkOrderId { get; set; }
    }
}
