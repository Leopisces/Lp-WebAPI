﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using NLog.Config;
using System;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace Lp.LogAnalysis
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 日志注入的扩展类
        /// </summary>
        /// <param name="service"></param>
        /// <param name="configuration">配置</param>
        /// <param name="Version">应用程序版本号(GetType().Assembly.GetName().Version.ToString())</param>
        /// <returns></returns>
        public static IServiceCollection AddMisServerLog(this IServiceCollection service, IConfiguration configuration, AssemblyName AssemblyName)
        {
            var storage = configuration.GetSection("MisLog");
            var ProjectInfo = configuration.GetSection("ProjectInfo");
            Console.WriteLine("storage:" + storage);
            Console.WriteLine("storage2:" + storage["MongoDBAddress"]);
            EnsureNlogConfig(AppDomain.CurrentDomain.BaseDirectory + "nlog.config", storage["MongoDBAddress"], storage["ESAddress"], ProjectInfo["ProjectName"]);

            service.AddSingleton<ILogClient>(factory =>
            {
                EventOptions eventOptions = new EventOptions();
                eventOptions.ProjectName = ProjectInfo["ProjectName"].ToString();
                eventOptions.ProjectLabel = ProjectInfo["ProjectLabel"].ToString();
                eventOptions.MongoDB = storage["MongoDBAddress"].ToString();
                if ((storage["ESAddress"] != null) && !string.IsNullOrEmpty(storage["ESAddress"]))
                    eventOptions.ES = storage["ESAddress"].ToString();
                eventOptions.Version = AssemblyName.Version.ToString();
                eventOptions.AssemblyNamePath = AssemblyName.FullName.ToString();
                var client = new LogClient(eventOptions);
                return client;
            });
            return service;
        }


        /// 确保NLog配置文件sql连接字符串正确,主要是为了判断系统日志是走mongdb还是es
        /// </summary>
        /// <param name="nlogPath"></param>
        /// <param name="sqlConnectionStr"></param>
        public static void EnsureNlogConfig(string nlogPath, string MogodbConnectionStr, string ESConectionStr, string index)
        {
            XDocument xd = XDocument.Load(nlogPath);
            if (!string.IsNullOrEmpty(ESConectionStr))
            {
                if (xd.Root.Elements().FirstOrDefault(a => a.Name.LocalName == "targets") is XElement targetsNode && targetsNode != null &&
                    targetsNode.Elements().FirstOrDefault(a => a.Name.LocalName == "target" && a.Attribute("name").Value == "ElasticSearch") is XElement targetNode && targetNode != null)
                {
                    targetNode.Attribute("uri").Value = ESConectionStr;
                    targetNode.Attribute("index").Value = index;
                }
                if (xd.Root.Elements().Count() > 0 && xd.Root.Elements().FirstOrDefault(a => a.Name.LocalName == "targets") != null)
                {
                    XElement root = (XElement)((XElement)xd.Root.Elements().FirstOrDefault(a => a.Name.LocalName == "targets")).Elements().FirstOrDefault(a => a.Name.LocalName == "target" && a.Attribute("name").Value == "MongoDB");
                    if (root != null)
                        root.Remove();
                }
                if (xd.Root.Elements().Count() > 0 && xd.Root.Elements().FirstOrDefault(a => a.Name.LocalName == "rules") != null)
                {
                    XElement rule = (XElement)((XElement)xd.Root.Elements().FirstOrDefault(a => a.Name.LocalName == "rules")).Elements().FirstOrDefault(a => a.Name.LocalName == "logger" && a.Attribute("writeTo").Value == "MongoDB");
                    if (rule != null)
                        rule.Remove();
                }
            }
            else
            {
                if (xd.Root.Elements().FirstOrDefault(a => a.Name.LocalName == "targets") is XElement targetsNode && targetsNode != null &&
                    targetsNode.Elements().FirstOrDefault(a => a.Name.LocalName == "target" && a.Attribute("name").Value == "MongoDB") is XElement targetNode && targetNode != null)
                {
                    targetNode.Attribute("connectionString").Value = MogodbConnectionStr;
                    targetNode.Attribute("databaseName").Value = "SysLog";
                    targetNode.Attribute("collectionName").Value = "Log";
                }
                if (xd.Root.Elements().Count() > 0 && xd.Root.Elements().FirstOrDefault(a => a.Name.LocalName == "targets") != null)
                {
                    XElement root = (XElement)((XElement)xd.Root.Elements().FirstOrDefault(a => a.Name.LocalName == "targets")).Elements().FirstOrDefault(a => a.Name.LocalName == "target" && a.Attribute("name").Value == "ElasticSearch");
                    if (root != null)
                        root.Remove();
                }
                if (xd.Root.Elements().Count() > 0 && xd.Root.Elements().FirstOrDefault(a => a.Name.LocalName == "rules") != null)
                {
                    XElement rule = (XElement)((XElement)xd.Root.Elements().FirstOrDefault(a => a.Name.LocalName == "rules")).Elements().FirstOrDefault(a => a.Name.LocalName == "logger" && a.Attribute("writeTo").Value == "ElasticSearch");
                    if (rule != null)
                        rule.Remove();
                }
            }
            xd.Save(nlogPath);
            //编辑后重新载入配置文件（不依靠NLog自己的autoReload，有延迟）
            LogManager.Configuration = new XmlLoggingConfiguration(nlogPath);
        }
    }
}
