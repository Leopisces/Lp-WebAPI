﻿using System;


namespace Lp.LogAnalysis
{
    /// <summary>
    /// 配置文件中的model
    /// </summary>
    public class EventOptions
    {
        /// <summary>
        /// 项目名称属性
        /// </summary>
        private string projectName = string.Empty;
        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName
        {
            get => projectName;
            set
            {
                if (value.IndexOf('.') > 0 || value.IndexOf(' ') > 0)
                    throw new ArgumentOutOfRangeException("The project name doesn't contains \".\"  or space");
                projectName = value;
            }
        }
        /// <summary>
        /// 项目标题
        /// </summary>
        public string ProjectLabel { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// API项目的路径
        /// </summary>
        public string AssemblyNamePath { get; set; }
        /// <summary>
        /// MongoDB数据库地址
        /// </summary>
        public string MongoDB { get; set; }
        /// <summary>
        /// ES数据存储地址
        /// </summary>
        public string ES { get; set; }
    }
}
