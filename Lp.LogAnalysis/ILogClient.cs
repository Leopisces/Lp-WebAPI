﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Lp.LogAnalysis
{
    /// <summary>
    /// 日志接口,调用异步时,不要使用Start
    /// </summary>
    public interface ILogClient
    {
        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="message">信息</param>
        /// <param name="extra">扩展信息</param>
        /// <returns></returns>
        void WriteDebug(string message, params object[] extra);
        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        void WriteDebug(string message, Exception exception);
        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="tag">标签,默认info</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        void WriteInfo(string message, params object[] extra);
        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        void WriteInfo(string message, Exception exception);
        /// <summary>
        /// 跟踪信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        void WriteTrace(string message, params object[] extra);
        /// <summary>
        /// 跟踪信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        void WriteTrace(string message, Exception exception);
        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        void WriteWarn(string message, params object[] extra);
        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        void WriteWarn(string message, Exception exception);
        /// <summary>
        /// 错误
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        void WriteError(string message, params object[] extra);

        /// <summary>
        /// 错误
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        void WriteError(string message, Exception exception);

        /// <summary>
        /// 致命信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        void WriteFatal(string message, params object[] extra);
        /// <summary>
        /// 致命信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        void WriteFatal(string message, Exception exception);
        /// <summary>
        /// 新增记录的日志
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        void WriteInsert<T>(string message, List<T> data, string oper, object extra = null);

        /// <summary>
        /// 新增记录的日志(异步)
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        Task<int> WriteInsertAsyc<T>(string message, List<T> data, string oper, object extra = null);
        /// <summary>
        /// 新增单个
        /// </summary>

        /// <summary>
        /// 修改记录的日志
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        void WriteUpdate<T>(string message, List<T> data, string oper, object extra = null);
        /// <summary>
        /// 修改记录的日志(异步)
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        Task<int> WriteUpdateAsyc<T>(string message, List<T> data, string oper, object extra = null);
        /// <summary>
        /// 物理删除记录日志
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        void WriteDelete<T>(string message, List<T> data, string oper, object extra = null);
        /// <summary>
        /// 物理删除记录日志(异步)
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        Task<int> WriteDeleteAsyc<T>(string message, List<T> data, string oper, object extra = null);
        /// <summary>
        /// 物理删除单个
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="data"></param>
        /// <param name="oper"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        void WriteDelete<T>(string message, T data, string oper, object extra = null);
        /// <summary>
        /// 物理删除记录日志(异步)
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        Task<int> WriteDeleteAsyc<T>(string message, T data, string oper, object extra = null);
        /// <summary>
        /// 数据库操作事件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="oper"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        void WriteDBEvent(string message, string oper, object extra = null, string tag = null);
        /// <summary>
        /// 数据库操作事件
        /// </summary>
        /// <param name="workOrderId"></param>
        /// <param name="message"></param>
        /// <param name="oper"></param>
        /// <param name="extra"></param>
        /// <param name="tag"></param>
        /// <param name="isNew"></param>
        void WriteDBEvent<T>(int workOrderId, string message, string oper, List<T> t, string tag = null) where T : class, new();
        /// <summary>
        /// 数据库操作事件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="workOrderId"></param>
        /// <param name="message"></param>
        /// <param name="oper"></param>
        /// <param name="t"></param>
        /// <param name="tag"></param>
        /// <param name="isNew"></param>
        void WriteDBEvent<T>(int workOrderId, string message, string oper, T t, string tag = null) where T : class, new();
        /// <summary>
        /// 数据库操作事件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <param name="exp"></param>
        /// <param name="tag"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        List<T> ReadDBEvent<T, D>(Expression<Func<T, bool>> exp, string tag, int pageIndex, int pageSize, out long total) where T : MogoMessage<D>;
        /// <summary>
        /// 数据库操作事件(异步)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="oper"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        Task<int> WriteDBEventAsyc(string message, string oper, object extra = null, string tag = null);
        /// <summary>
        /// 执行的sql日志
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="sql">sql语句</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        void WriteSQL(string message, string sql, string oper, object extra = null);
        ///// <summary>
        ///// 执行的sql日志(异步)
        ///// </summary>
        ///// <param name="message">消息</param>
        ///// <param name="sql">sql语句</param>
        ///// <param name="oper">操作人</param>
        ///// <param name="extra">扩展内容</param>
        ///// <returns></returns>
        //Task<int> WriteSQLAsyc(string message, string sql, string oper, object extra = null);
        /// <summary>
        /// 登陆信息
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        void WriteLogin<T>(string message, T data, string oper, object extra = null);
        /// <summary>
        /// 登陆信息(异步)
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        Task<int> WriteLoginAysc<T>(string message, T data, string oper, object extra = null);
        /// <summary>
        /// 业务日志
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">数据列表</param>
        /// <param name="oper">创建人</param>
        /// <param name="tag">默认Bus,但是每个业务有个固定的标签,比如流向表等</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        void WriteBus<T>(string message, List<T> data, string oper, string tag = "Bus", object extra = null);
        /// <summary>
        /// 业务日志(异步)
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">数据列表</param>
        /// <param name="oper">创建人</param>
        /// <param name="tag">默认Bus,但是每个业务有个固定的标签,比如流向表等</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        Task<int> WriteBusAsyc<T>(string message, List<T> data, string oper, string tag = "Bus", object extra = null);
    }
}
