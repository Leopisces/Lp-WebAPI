﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Lp.LogAnalysis
{
    public class LogClient : ILogClient
    {
        /// <summary>
        /// MongoClient
        /// </summary>
        private readonly MongoClient modbclient;
        /// <summary>
        /// 服务实体
        /// </summary>
        private EventOptions Options { get; set; }
        /// <summary>
        /// 初始化日志
        /// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger(); //初始化日志类

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        /// <param name="lessExceptionLog"></param>
        public LogClient(EventOptions options)
        {
            this.Options = options;
            modbclient = new MongoClient(options.MongoDB);
            var serializer = new MongoDB.Bson.Serialization.Serializers.DateTimeSerializer(DateTimeKind.Local, BsonType.DateTime);
            BsonSerializer.RegisterSerializer(typeof(DateTime), serializer);
        }

        #region 系统日志
        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="message">信息</param>
        /// <param name="extra">扩展信息</param>
        /// <returns></returns>
        public void WriteDebug(string message, params object[] extra)
        {
            logger.Debug(message, extra);
        }
        /// <summary>
        /// 调试信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void WriteDebug(string message, Exception exception)
        {
            logger.Debug(exception, message);
        }
        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="tag">标签,默认info</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        /// <returns></returns>
        public void WriteInfo(string message, params object[] extra)
        {
            logger.Info(message, extra);
        }
        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        public void WriteInfo(string message, Exception exception)
        {
            logger.Info(exception, message);
        }


        /// <summary>
        /// 跟踪信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        public void WriteTrace(string message, params object[] extra)
        {
            logger.Trace(message, extra);
        }

        /// <summary>
        /// 跟踪信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        public void WriteTrace(string message, Exception exception)
        {
            logger.Trace(exception, message);
        }

        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        public void WriteWarn(string message, params object[] extra)
        {
            logger.Warn(message, extra);
        }
        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        public void WriteWarn(string message, Exception exception)
        {
            logger.Warn(exception, message);
        }
        /// <summary>
        /// 错误
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        public void WriteError(string message, params object[] extra)
        {
            try
            {
                logger.Error(message, extra);
            }
            catch (Exception ex)
            {
                Console.WriteLine("记录错误日志出错" + ex.Message);
            }

        }
        /// <summary>
        /// 错误
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        public void WriteError(string message, Exception exception)
        {
            logger.Error(exception, message);
        }

        /// <summary>
        /// 致命信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        public void WriteFatal(string message, params object[] extra)
        {
            logger.Fatal(message, extra);
        }
        /// <summary>
        /// 致命信息
        /// </summary>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        public void WriteFatal(string message, Exception exception)
        {
            logger.Fatal(exception, message);
        }
        #endregion

        #region 业务日志
        /// <summary>
        /// 新增记录的日志
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public void WriteInsert<T>(string message, List<T> data, string oper, object extra = null)
        {
            string tag = "Insert";
            WriteMogoDB(message, tag, data, oper, extra);
        }
        /// <summary>
        /// 新增记录的日志(异步)
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public async Task<int> WriteInsertAsyc<T>(string message, List<T> data, string oper, object extra = null)
        {
            string tag = "Insert";
            return await WriteMogoDBAsync(message, tag, data, oper, extra);
        }
        /// <summary>
        /// 修改记录的日志
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public void WriteUpdate<T>(string message, List<T> data, string oper, object extra = null)
        {
            string tag = "Update";
            WriteMogoDB(message, tag, data, oper, extra);
        }
        /// <summary>
        /// 修改记录的日志(异步)
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public async Task<int> WriteUpdateAsyc<T>(string message, List<T> data, string oper, object extra = null)
        {
            string tag = "Update";
            return await WriteMogoDBAsync(message, tag, data, oper, extra);
        }
        /// <summary>
        /// 物理删除记录日志
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public void WriteDelete<T>(string message, List<T> data, string oper, object extra = null)
        {
            string tag = "Delete";
            WriteMogoDB(message, tag, data, oper, extra);
        }
        /// <summary>
        /// 物理删除记录日志(异步)
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public async Task<int> WriteDeleteAsyc<T>(string message, List<T> data, string oper, object extra = null)
        {
            string tag = "Delete";
            return await WriteMogoDBAsync(message, tag, data, oper, extra);
        }
        /// <summary>
        /// 物理删除记录日志
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public void WriteDelete<T>(string message, T data, string oper, object extra = null)
        {
            string tag = "Delete";
            List<T> list = new List<T>();
            list.Add(data);
            WriteMogoDB(message, tag, list, oper, extra);
        }
        /// <summary>
        /// 物理删除记录日志(异步)
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public async Task<int> WriteDeleteAsyc<T>(string message, T data, string oper, object extra = null)
        {
            string tag = "Delete";
            List<T> list = new List<T>();
            list.Add(data);
            return await WriteMogoDBAsync(message, tag, list, oper, extra);
        }
        /// <summary>
        /// 数据库操作事件(异步)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="oper"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        public async Task<int> WriteDBEventAsyc(string message, string oper, object extra = null, string tag = null)
        {
            return await WriteMogoDBAsync<object>(message, tag, null, oper, extra);
        }
        /// <summary>
        /// 数据库操作事件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="oper"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        public void WriteDBEvent(string message, string oper, object extra = null, string tag = null)
        {
            WriteMogoDB<object>(message, tag, null, oper, extra);
        }

        /// <summary>
        /// 数据库操作事件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="oper"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        public void WriteDBEvent<T>(int workOrderId, string message, string oper, List<T> t, string tag = null) where T : class, new()
        {
            WriteMogoDB(workOrderId, message, tag, t, oper, null);
        }

        /// <summary>
        /// 数据库操作事件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="oper"></param>
        /// <param name="extra"></param>
        /// <returns></returns>
        public void WriteDBEvent<T>(int workOrderId, string message, string oper, T t, string tag = null) where T : class, new()
        {
            var list = new List<T>();
            list.Add(t);
            WriteMogoDB(workOrderId, message, tag, list, oper, null);
        }

        /// <summary>
        /// 数据库操作事件
        /// </summary>
        /// <param name="workOrderId"></param>
        /// <param name="message"></param>
        /// <param name="oper"></param>
        /// <param name="extra"></param>
        /// <param name="tag"></param>
        /// <param name="isNew"></param>
        public List<T> ReadDBEvent<T, D>(Expression<Func<T, bool>> exp, string tag, int pageIndex, int pageSize, out long total) where T : MogoMessage<D>
        {
            return ReadMogoDB<T, D>(exp, tag, pageIndex, pageSize, out total);
        }

        /// <summary>
        /// 执行的sql日志
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="sql">sql语句</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public void WriteSQL(string message, string sql, string oper, object extra = null)
        {
            string tag = "SQLExec";
            List<string> str = new List<string>();
            str.Add(sql);
            WriteMogoDB(message, tag, str, oper, extra);
        }
        /// <summary>
        /// 执行的sql日志(异步)
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="sql">sql语句</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public async Task<int> WriteSQLAsyc(string message, string sql, string oper, object extra = null)
        {
            string tag = "SQLExec";
            List<string> str = new List<string>();
            str.Add(sql);
            return await WriteMogoDBAsync(message, tag, str, oper, extra);
        }
        /// <summary>
        /// 登陆信息
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public void WriteLogin<T>(string message, T data, string oper, object extra = null)
        {
            string tag = "Login";
            List<T> list = new List<T>();
            list.Add(data);
            WriteMogoDB(message, tag, list, oper, extra);
        }

        /// <summary>
        /// 登陆信息(异步)
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">实体列表</param>
        /// <param name="oper">操作人</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public async Task<int> WriteLoginAysc<T>(string message, T data, string oper, object extra = null)
        {
            string tag = "Login";
            List<T> list = new List<T>();
            list.Add(data);
            return await WriteMogoDBAsync(message, tag, list, oper, extra);
        }
        /// <summary>
        /// 业务日志
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">数据列表</param>
        /// <param name="oper">创建人</param>
        /// <param name="tag">默认Bus,但是每个业务有个固定的标签,比如流向表等</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public void WriteBus<T>(string message, List<T> data, string oper, string tag = "Bus", object extra = null)
        {
            WriteMogoDB(message, tag, data, oper, extra);
        }
        /// <summary>
        /// 业务日志(异步)
        /// </summary>
        /// <typeparam name="T">实体模型</typeparam>
        /// <param name="message">消息</param>
        /// <param name="data">数据列表</param>
        /// <param name="oper">创建人</param>
        /// <param name="tag">默认Bus,但是每个业务有个固定的标签,比如流向表等</param>
        /// <param name="extra">扩展内容</param>
        /// <returns></returns>
        public async Task<int> WriteBusAsyc<T>(string message, List<T> data, string oper, string tag = "Bus", object extra = null)
        {
            return await WriteMogoDBAsync(message, tag, data, oper, extra);
        }
        #endregion

        /// <summary>
        /// 专门用于业务日志的
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <param name="extramsg"></param>
        /// <returns></returns>
        private void WriteMogoDB<T>(string message, string tag, List<T> t, string oper, object extra = null)
        {
            MogoMessage<T> log = CreateLog(t, message, tag, oper, extra);
            try
            {
                var db = modbclient.GetDatabase(Options.ProjectName.ToString());
                var collections = db.GetCollection<BsonDocument>(tag);
                var doc = log.ToBsonDocument();
                collections.InsertOneAsync(doc);
            }
            catch (Exception e)
            {
                WriteError(tag + message + "WriteMogoDB", e);
            }
        }

        /// <summary>
        /// 专门用于业务日志的
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="workOrderId"></param>
        /// <param name="message"></param>
        /// <param name="tag"></param>
        /// <param name="t"></param>
        /// <param name="oper"></param>
        /// <param name="extra"></param>
        /// <param name="isNew"></param>
        private void WriteMogoDB<T>(int workOrderId, string message, string tag, List<T> t, string oper, object extra = null)
        {
            MogoMessage<T> log = CreateLog(t, workOrderId, message, tag, oper, extra);
            try
            {
                var db = modbclient.GetDatabase(Options.ProjectName.ToString());
                var collections = db.GetCollection<MogoMessage<T>>(tag);
                //var doc = log.ToBsonDocument();
                collections.InsertOneAsync(log);
            }
            catch (Exception e)
            {
                WriteError(tag + message + "WriteMogoDB", e);
            }
        }

        /// <summary>
        /// ReadMogoDB
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="workOrderId"></param>
        /// <param name="message"></param>
        /// <param name="tag"></param>
        /// <param name="t"></param>
        /// <param name="oper"></param>
        /// <param name="extra"></param>
        /// <param name="isNew"></param>
        public List<T> ReadMogoDB<T, D>(Expression<Func<T, bool>> exp, string tag, int pageIndex, int pageSize, out long total) where T : MogoMessage<D>
        {
            try
            {
                var db = modbclient.GetDatabase(Options.ProjectName.ToString());
                var collections = db.GetCollection<T>(tag);
                var sort = Builders<T>.Sort;
                total = collections.Find(exp).CountDocuments();
                var list = collections.Find(exp).Sort(sort.Descending(c => c.CreateTime)).Skip((pageIndex - 1) * pageSize).Limit(pageSize).ToList();
                return list;

            }
            catch (Exception e)
            {
                WriteError(tag + "ReadMogoDB", e);
                total = 0;
                return null;
            }
        }

        /// <summary>
        /// 专门用于业务日志的(异步)
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="message"></param>
        /// <param name="extra"></param>
        /// <param name="extramsg"></param>
        /// <returns></returns>
        private async Task<int> WriteMogoDBAsync<T>(string message, string tag, List<T> t, string oper, object extra = null)
        {
            MogoMessage<T> log = CreateLog(t, message, tag, oper, extra);
            int affrows = 0;
            try
            {
                var db = modbclient.GetDatabase(Options.ProjectName.ToString());
                var collections = db.GetCollection<BsonDocument>(tag);
                var doc = log.ToBsonDocument();
                await collections.InsertOneAsync(doc);
                affrows++;
            }
            catch (Exception e)
            {
                WriteError(tag + message + "WriteMogoDB", e);
            }
            return affrows;
        }
        /// <summary>
        /// 创建业务日志模板
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="tag"></param>
        /// <param name="oper"></param>
        /// <returns></returns>
        private MogoMessage<T> CreateLog<T>(List<T> t, string message, string tag, string oper, object extra = null)
        {
            var ips = GetHostAddress();
            var hostName = Dns.GetHostName();
            MogoMessage<T> mogoMessage = new MogoMessage<T>();
            mogoMessage.Id = Guid.NewGuid().ToString();
            mogoMessage.Tag = tag;
            mogoMessage.IP = ips.FirstOrDefault();
            mogoMessage.Message = message;
            mogoMessage.MachineName = hostName;
            mogoMessage.DataBase = Options.ProjectName;
            mogoMessage.CreateOper = oper;
            mogoMessage.CreateTime = DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss");
            mogoMessage.Data = t;
            mogoMessage.Extra = extra;
            return mogoMessage;
        }

        /// <summary>
        /// 创建业务日志模板
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="workOrderId"></param>
        /// <param name="message"></param>
        /// <param name="tag"></param>
        /// <param name="oper"></param>
        /// <param name="extra"></param>
        /// <param name="isNew"></param>
        /// <returns></returns>
        private MogoMessage<T> CreateLog<T>(List<T> t, int workOrderId, string message, string tag, string oper, object extra = null)
        {
            var ips = GetHostAddress();
            var hostName = Dns.GetHostName();
            MogoMessage<T> mogoMessage = new MogoMessage<T>();
            mogoMessage.Id = Guid.NewGuid().ToString();
            mogoMessage.Tag = tag;
            mogoMessage.IP = ips.FirstOrDefault();
            mogoMessage.Message = message;
            mogoMessage.MachineName = hostName;
            mogoMessage.DataBase = Options.ProjectName;
            mogoMessage.CreateOper = oper;
            mogoMessage.CreateTime = DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss");
            mogoMessage.Data = t;
            mogoMessage.Extra = extra;
            mogoMessage.WorkOrderId = workOrderId;
            return mogoMessage;
        }

        /// <summary>
        /// 获取本机ip
        /// </summary>
        /// <returns></returns>
        public static List<string> GetHostAddress()
        {
            List<string> result = new List<string>();
            var ipList = NetworkInterface.GetAllNetworkInterfaces()
                                         .Where(f => f.NetworkInterfaceType != NetworkInterfaceType.Loopback && f.OperationalStatus == OperationalStatus.Up).ToList();
            foreach (var item in ipList)
            {
                var props = item.GetIPProperties();
                var ipv4 = props.UnicastAddresses
                                .Where(f => f.Address.AddressFamily == AddressFamily.InterNetwork)
                                .Select(f => f.Address)
                                .FirstOrDefault();

                if (ipv4 == null) continue;
                result.Add(ipv4.ToString());
            }
            return result;
        }
    }
}
