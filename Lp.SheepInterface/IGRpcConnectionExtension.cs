﻿using Lp.Grpc;
using System;

namespace Lp.SheepInterface
{
    public static class IGRpcConnectionExtension
    {
        //static ILogClient logClient;

        static IGRpcConnectionExtension()
        {
            //logClient = DIContainer.ServiceLocator.Instance.GetService<ILogClient>();
        }

        /// <summary>
        /// SheepGRPC
        /// </summary>
        private static string SheepGRPC = "Sheep_GRPC";

        public static ITest_GRPC Get_ITest_GRPC(this IGRpcConnection _iGRpcConnection)
        {
            try
            {

                var rs = _iGRpcConnection.GetRemoteService<ITest_GRPC>(SheepGRPC).Result;
                if (rs == null)
                {
                    //logClient.WriteError("Get_FieldsLimit_GRPC - 返回空" + DateTime.Now.ToLocalTime().ToString());
                    throw new Exception($"{SheepGRPC} 服务检测不到");
                }
                else
                    return rs;

            }
            catch (Exception ex)
            {
                //logClient.WriteError("IGRpcConnectionExtension," + ex.Message, ex);
                throw ex;
            }
        }

        //public static TService Get_GRPCService<TService>(this IGRpcConnection _iGRpcConnection, string Path) where TService : IService<TService>
        //{
        //    try
        //    {
        //        var rs = _iGRpcConnection.GetRemoteService<TService>(Path).Result;
        //        if (rs == null)
        //        {
        //            string name = typeof(TService).Name;
        //            //logClient.WriteError($"{name} - 返回空" + DateTime.Now.ToLocalTime().ToString());
        //            throw new Exception($"GRPC {name} 服务检测不到");
        //        }
        //        else
        //            return rs;
        //    }
        //    catch (Exception ex)
        //    {
        //        //logClient.WriteError("IGRpcConnectionExtension," + ex.Message, ex);
        //        throw ex;
        //    }
        //}

    }
}
