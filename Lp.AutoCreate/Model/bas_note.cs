﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Authentication.Model
{
    ///<summary>
    ///笔记本
    ///</summary>
    [SugarTable("bas_note")]
    public partial class bas_note
    {
           public bas_note(){


           }
           /// <summary>
           /// Desc:主键
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int note_id {get;set;}

           /// <summary>
           /// Desc:上级主键
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? parent_id {get;set;}

           /// <summary>
           /// Desc:标题
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string title {get;set;}

           /// <summary>
           /// Desc:内容
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string context {get;set;}

           /// <summary>
           /// Desc:创建人编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? create_id {get;set;}

           /// <summary>
           /// Desc:创建日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? create_date {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string create_name {get;set;}

           /// <summary>
           /// Desc:修改人编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? modify_id {get;set;}

           /// <summary>
           /// Desc:修改日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? modify_date {get;set;}

           /// <summary>
           /// Desc:修改人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string modify_name {get;set;}

           /// <summary>
           /// Desc:是否有效
           /// Default:b'1'
           /// Nullable:True
           /// </summary>           
           public bool? enabled {get;set;}

    }
}
