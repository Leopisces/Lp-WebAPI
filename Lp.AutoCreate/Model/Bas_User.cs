﻿using System;
using System.Linq;
using System.Text;
using SqlSugar;

namespace Authentication.Model
{
    ///<summary>
    ///用户表
    ///</summary>
    [SugarTable("Bas_User")]
    public partial class Bas_User
    {
           public Bas_User(){


           }
           /// <summary>
           /// Desc:用户编码
           /// Default:
           /// Nullable:False
           /// </summary>           
           [SugarColumn(IsPrimaryKey=true,IsIdentity=true)]
           public int User_ID {get;set;}

           /// <summary>
           /// Desc:昵称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Nick_Name {get;set;}

           /// <summary>
           /// Desc:名称
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Name {get;set;}

           /// <summary>
           /// Desc:密码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Password {get;set;}

           /// <summary>
           /// Desc:创建人编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Create_ID {get;set;}

           /// <summary>
           /// Desc:创建日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? Create_Date {get;set;}

           /// <summary>
           /// Desc:创建人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Create_Name {get;set;}

           /// <summary>
           /// Desc:修改人编码
           /// Default:
           /// Nullable:True
           /// </summary>           
           public int? Modify_ID {get;set;}

           /// <summary>
           /// Desc:修改日期
           /// Default:
           /// Nullable:True
           /// </summary>           
           public DateTime? Modify_Date {get;set;}

           /// <summary>
           /// Desc:修改人
           /// Default:
           /// Nullable:True
           /// </summary>           
           public string Modify_Name {get;set;}

           /// <summary>
           /// Desc:是否有效
           /// Default:b'1'
           /// Nullable:True
           /// </summary>           
           public bool? Enable {get;set;}

    }
}
