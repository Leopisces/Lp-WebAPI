﻿using Lp.SqlSugarORM;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Lp.AutoCreate
{
    public class AutoCreateModelClass : BaseRepository
    {
        public AutoCreateModelClass(IOptions<SugarOption> options) : base(options)
        {

        }

        protected override void init()
        {
            DatabaseEnum = SqlDatabaseEnum.PRIMARY;
        }

        /// <summary>
        /// 自动生成model
        /// </summary>
        public void CreateModel()
        {
            var db = Db;
            var path = Directory.GetCurrentDirectory() + "../../../../";
            foreach (var p in sugarOption.AutoCreate.ModelList)
            {
                foreach(var c in p.Value)
                {
                    db.DbFirst.Where(it => it == c).IsCreateAttribute().CreateClassFile(path + "\\Model", p.Key);
                    Console.WriteLine($"{c}.cs已生成");
                }
            }
        }
    }
}
