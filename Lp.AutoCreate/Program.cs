﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Text;
using System.Threading;
using Lp.Core.DI;
using Lp.SqlSugarORM;

namespace Lp.AutoCreate
{
    class Program
    {
        static void Main(string[] args)
        {
            //编码注册
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            //配置文件
            var builder = new ConfigurationBuilder()
                //.SetBasePath(Directory.GetCurrentDirectory())
                .SetBasePath(Directory.GetCurrentDirectory() + "../../../../")
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            IConfigurationRoot configuration = builder.Build();

            //依赖注入
            IServiceCollection services = new ServiceCollection();
            services.AddOptions();

            //注入SqlSugar配置
            services.Configure<SugarOption>(configuration.GetSection("Sugar"));

            //自动生成model
            services.AddSingleton<AutoCreateModelClass>();

            //注入IConfiguration
            services.AddSingleton<IConfiguration>(configuration);
            IServiceProvider serviceProvider = services.BuildServiceProvider();
            DIContainer.ServiceLocator.Instance = serviceProvider;

            //自动生成Model
            var autoCreate = serviceProvider.GetService<AutoCreateModelClass>();
            autoCreate.CreateModel();
            Console.WriteLine("Model已自动生成");
        }
    }
}
