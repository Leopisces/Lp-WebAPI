﻿using Lp.Core.Helper;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Lp.AutoCreate
{
    public class AutoCreateByTemplate
    {
        public static void Create(string templatePath, string savePath, object model)
        {
            string template = File.ReadAllText(templatePath); //从文件中读出模板内容
            string templateKey = "dbcontext"; //取个名字
            var result = Engine.Razor.RunCompile(template, templateKey, model.GetType(), model);
            FileHelper.CreateFile(savePath, result, System.Text.Encoding.UTF8);
        }
    }
}
