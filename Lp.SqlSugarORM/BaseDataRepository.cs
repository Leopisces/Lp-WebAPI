﻿using Lp.Core.Entity;
using Lp.LogAnalysis;
using Microsoft.Extensions.Options;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace Lp.SqlSugarORM
{
    /// <summary>
    /// 通用的读方法 partial
    /// </summary>
    public abstract partial class BaseDataRepository<T> : BaseRepository, IBaseDataRepository<T> where T : class, new()
    {
        /// <summary>
        /// init
        /// </summary>
        public BaseDataRepository(IOptions<SugarOption> options) : base(options)
        {

        }
        /// <summary>
        /// 获取有变化的字段
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        protected string[] GetValidColumns(T obj)
        {
            List<string> list = new List<string>();
            var type = typeof(T);
            foreach (var pro in type.GetProperties())
            {
                var data = pro.GetValue(obj);
                if (data != null)
                {
                    list.Add(pro.Name);
                }
                if (data != null && data.ToString() == "#NULL#")
                {
                    pro.SetValue(obj, null);
                }
            }
            return list.ToArray();

        }

        protected string TypeName
        {
            get
            {
                return typeof(T).Name;
            }
        }
        public async Task<bool> Delete(T obj, IClientInformation client)
        {
            var r = await Task.Run(() => this.Db.Deleteable(obj).ExecuteCommand());
            //_log.WriteDBEvent("Delete", client.Code + client.RealName, obj, "Delete");
            return r > 0;
        }

        public async Task<bool> Delete(object key, IClientInformation client)
        {
            var Db = this.Db;
            var obj = Db.Queryable<T>().InSingle(key);
            var r = await Task.Run(() => Db.Deleteable<T>(obj).ExecuteCommand());
            //_log.WriteDBEvent("Delete", client.Code + client.RealName, obj, "Delete");
            return r > 0;
        }

        public async Task<bool> Deletetable(Expression<Func<T, bool>> exprission, IClientInformation client)
        {
            using (var Db = this.Db)
            {
                var obj = Db.Queryable<T>().Where(exprission).ToList();
                var r = await Task.Run(() => Db.Deleteable<T>().Where(exprission).ExecuteCommand());
                //_log.WriteDBEvent("Delete", client.Code + client.RealName, obj, "Delete");
                return r > 0;
            }

        }

        public async Task<bool> Deletetable(List<T> list, IClientInformation client)
        {

            if (list.Count > 0)
            {
                var r = await Task.Run(() => Db.Deleteable(list).ExecuteCommand());
                //_log.WriteDBEvent("Delete", client.Code + client.RealName, list, "Deletetable");
                return r > 0;
            }
            else
                return false;

        }

        public async Task<bool> Deletetable(List<object> keys, IClientInformation client)
        {
            if (keys.Count > 0)
            {
                var Db = this.Db;
                var objs = Db.Queryable<T>().In(keys).ToList();
                var r = await Task.Run(() => Db.Deleteable(objs).ExecuteCommand());
                //_log.WriteDBEvent("Delete", client.Code + client.RealName, objs, "Deletetable");
                return r > 0;
            }
            else
                return false;

        }

        public async Task<bool> Insert(T obj, IClientInformation client)
        {
            var r = await Task.Run(() => Db.Insertable(obj).ExecuteCommand());
            //_log.WriteDBEvent("Insert", client.Code + client.RealName, obj, "Insert");

            return r > 0;
        }

        public async Task<bool> Insert(List<T> obj, IClientInformation client)
        {
            var r = await Task.Run(() => Db.Insertable(obj).ExecuteCommand());
            //_log.WriteDBEvent("Insert", client.Code + client.RealName, obj, "Insert");

            return r > 0;
        }

        public async Task<bool> Insertable(List<T> list, IClientInformation client)
        {

            var Db = this.Db;
            //解决插入数据过多导致的报错
            if (list.Count > 2000)
            {
                try
                {
                    Db.BeginTran();
                    //分片插入
                    int step = 2000;
                    int length = (list.Count % step == 0 ? list.Count / step : (list.Count / step) + 1);
                    for (int i = 0; i < length; i++)
                    {
                        //取剩余的记录
                        int getcount = step;
                        if (list.Count % step != 0 && i + 1 == length)
                        {
                            getcount = list.Count % step;
                        }
                        var r = await Task.Run(() => Db.Insertable(list.GetRange(i * step, getcount)).ExecuteCommand());
                        if (r <= 0)
                        {
                            throw new Exception("插入记录失败!");
                        }
                    }
                    Db.CommitTran();
                    return true;
                }
                catch
                {
                    Db.RollbackTran();
                    return false;
                }
            }
            else
            {
                if (list.Count > 0)
                {
                    var r = await Task.Run(() => Db.Insertable(list).ExecuteCommand());
                    //_log.WriteDBEvent("Insert", client.Code + client.RealName, list, "Insertable");
                    return r > 0;
                }
                else
                    return false;
            }
        }
        public async Task<bool> Inserttable_BreifLog(List<T> list, IClientInformation client)
        {
            if (list.Count > 0)
            {
                var r = await Task.Run(() => Db.Insertable(list).ExecuteCommand());
                //_log.WriteDBEvent("Insert", client.Code + client.RealName, list.Count(), "Insertable With Brief Log");
                return r > 0;
            }
            else
                return false;

        }

        public async Task<int> InsertReturnInt(T obj, IClientInformation client)
        {
            var r = await Task.Run(() => Db.Insertable(obj).ExecuteReturnIdentity());
            //_log.WriteDBEvent("Insert", client.Code + client.RealName, obj, "InsertReturnInt");
            return r;
        }

        public async Task<long> InsertReturnLong(T obj, IClientInformation client)
        {
            var r = await Task.Run(() => Db.Insertable(obj).ExecuteReturnBigIdentity());
            //_log.WriteDBEvent("Insert", client.Code + client.RealName, obj, "InsertReturnLong");
            return r;
        }

        public async Task<bool> Update(T obj, string[] columns, IClientInformation client)
        {
            var _Db = this.Db;
            var key = GetPrimaryObj(obj);
            var old = key != null ? _Db.Queryable<T>().InSingle(GetPrimaryObj(obj)) : null;
            var r = await Task.Run(() => _Db.Updateable(obj).UpdateColumnsIF(columns.Length > 0, columns).ExecuteCommand());

            //_log.WriteDBEvent("Update", client.Code + client.RealName, new
            //{
            //    newObject = obj,
            //    oldObject = old,
            //    changedColumns = columns
            //}, "InsertReturnLong");

            return r > 0;
        }

        public async Task<bool> Updatetable(List<T> list, string[] columns, IClientInformation client)
        {
            //var _Db = this.Db;
            //var keys = GetPrimaryObj(list);
            //var old = _Db.Queryable<T>().In(keys);
            if (list.Count > 0)
            {
                try
                {
                    var r = await Task.Run(() => Db.Updateable(list).UpdateColumnsIF(columns.Length > 0, columns).ExecuteCommand());
                    //_log.WriteDBEvent("Update", client.Code + client.RealName, new
                    //{
                    //    newObject = list,
                    //    oldObject = old,
                    //    changedColumns = columns
                    //}, "Updatetable");
                    return r > 0;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }


            }
            return false;
        }

        public async Task<bool> Updatetable(List<T> list, IClientInformation client)
        {
            if (list.Count > 0)
            {
                try
                {
                    var r = await Task.Run(() => Db.Updateable(list).ExecuteCommand());
                    return r > 0;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());

                }
            }
            return false;
        }
        public async Task<bool> Update(T obj, IClientInformation client)
        {
            var _Db = this.Db;
            var key = GetPrimaryObj(obj);
            var old = key != null ? _Db.Queryable<T>().InSingle(GetPrimaryObj(obj)) : null;
            var columns = this.GetValidColumns(obj);
            var r = await Task.Run(() => Db.Updateable(obj).UpdateColumnsIF(columns.Length > 0, columns).ExecuteCommand());
            //_log.WriteDBEvent("Update", client.Code + client.RealName, new
            //{
            //    newObject = obj,
            //    oldObject = old,
            //    changedColumns = columns
            //}, "Update");
            return r > 0;
        }

        public async Task<T> Get(object key, IClientInformation client)
        {
            return await Task.Run(() => Db.Queryable<T>().InSingle(key));
        }

        public async Task<object> GetKey(T obj, IClientInformation client)
        {
            return await Task.Run(() => this.GetPrimaryObj(obj)); ;
        }

        public async Task<List<T>> ListByPrimaryArr(object[] arr)
        {
            return await Task.Run(() =>
            {
                var Db = this.Db;
                var PrimaryKey = GetPrimaryName();

                if (arr != null && arr.Count() > 0)
                {
                    if (!string.IsNullOrEmpty(PrimaryKey))
                    {
                        string where = string.Format("{0} in ({1})", PrimaryKey, string.Join(",", arr.Select(d => "'" + d.ToString() + "'").ToArray()));
                        return Db.Queryable<T>().Where(where).ToList();
                    }
                    else
                    {
                        throw new Exception("no Primary Key");
                    }
                }
                else
                    return new List<T>();
            });

        }

        public async Task<object> GetPrimaryObj(T obj)
        {
            return await Task.Run(() =>
            {
                var pros = typeof(T).GetProperties();
                foreach (var pro in pros)
                {
                    var attr = pro.GetCustomAttribute<SugarColumn>();
                    if (attr != null && attr.IsPrimaryKey)
                    {
                        return pro.GetValue(obj);
                    }
                }
                return null;
            });

        }

        public async Task<T> GetObjectByOldObject(T obj)
        {
            var _Db = this.Db;
            return await Task.Run(() =>
            {
                var key = GetPrimaryObj(obj);
                var old = key != null ? _Db.Queryable<T>().InSingle(GetPrimaryObj(obj)) : null;
                return old;
            });
        }

        private List<object> GetPrimaryObj(List<T> objs)
        {
            var keys = new List<object>();
            var pros = typeof(T).GetProperties();
            foreach (var pro in pros)
            {
                var attr = pro.GetCustomAttribute<SugarColumn>();
                if (attr.IsPrimaryKey && attr != null)
                {
                    foreach (var obj in objs)
                    {
                        keys.Add(pro.GetValue(obj));
                    }
                    return keys;
                }
            }
            return null;
        }

        private string GetPrimaryName()
        {
            var pros = typeof(T).GetProperties();
            foreach (var pro in pros)
            {
                var attr = pro.GetCustomAttribute<SugarColumn>();
                if (attr != null && attr.IsPrimaryKey)
                {
                    return pro.Name;
                }
            }
            return null;
        }
    }
}
