﻿using Lp.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Lp.SqlSugarORM
{
    public partial interface IBaseDataRepository<T> where T : class, new()
    {
        Task<bool> Insert(T obj, IClientInformation client);
        Task<bool> Insert(List<T> obj, IClientInformation client);
        Task<int> InsertReturnInt(T obj, IClientInformation client);
        Task<long> InsertReturnLong(T obj, IClientInformation client);
        Task<bool> Insertable(List<T> list, IClientInformation client);
        Task<bool> Inserttable_BreifLog(List<T> list, IClientInformation client);



       
        Task<bool> Update(T obj, string[] columns, IClientInformation client);
        Task<bool> Update(T obj, IClientInformation client);
        Task<bool> Updatetable(List<T> list, string[] columns, IClientInformation client);
        Task<bool> Updatetable(List<T> list,  IClientInformation client);


        Task<bool> Delete(T obj, IClientInformation client);
        Task<bool> Delete(object key, IClientInformation client);
        Task<bool> Deletetable(List<T> list, IClientInformation client);
        Task<bool> Deletetable(List<object> keys, IClientInformation client);

        Task<bool> Deletetable(Expression<Func<T, bool>> exprission, IClientInformation client);
    }
}
