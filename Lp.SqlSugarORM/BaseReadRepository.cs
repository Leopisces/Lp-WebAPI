﻿using Lp.Core.Entity;
using Microsoft.Extensions.Options;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace Lp.SqlSugarORM
{
    /// <summary>
    /// 通用的读方法 partial
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract partial class BaseDataRepository<T> : BaseRepository, IBaseDataRepository<T> where T : class, new()
    {
        /// <summary>
        /// 获取记录数
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public async Task<int> Count(Expression<Func<T, bool>> exception, string where)
        {
            return await Task.Run(() => Db.Queryable<T>().Where(exception).WhereIF(!string.IsNullOrEmpty(where), where).Count());
        }

        /// <summary>
        /// 获取单条记录(主键)
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<T> Get(object key)
        {
            return await Task.Run(() => Db.Queryable<T>().InSingle(key));
        }

        /// <summary>
        /// 获取单条记录(条件)
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<T> GetSingle(Expression<Func<T, bool>> expression)
        {
            return await Task.Run(() => Db.Queryable<T>().Where(expression).First());
        }

        /// <summary>
        /// 分页获取表格
        /// </summary>
        /// <param name="select"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public async Task<Tuple<DataTable, int>> GetTable(string[] select, int pageSize, int pageIndex)
        {
            string selectStr = string.Join(",", select);
            return await Task.Run(() =>
            {
                int total = 0;
                var q = Db.Queryable<T>()
                    .Select(selectStr)
                    .ToDataTablePage(pageIndex, pageSize, ref total);
                return new Tuple<DataTable, int>(q, total);

            });
        }

        /// <summary>
        /// 获取数据表格
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="select"></param>
        /// <returns></returns>
        public async Task<DataTable> GetTable(Expression<Func<T, bool>> expression, string[] select = null)
        {
            return await Task.Run(() =>
            {
                if (select == null)
                {
                    var q = Db.Queryable<T>()
                   .WhereIF(expression != null, expression)
                   .ToDataTable();
                    return q;
                }
                else
                {
                    var selectStr = string.Join(",", select);
                    var q = Db.Queryable<T>()
                      .WhereIF(expression != null, expression)
                      .Select(selectStr)
                      .ToDataTable();
                    return q;
                }


            });
        }

        /// <summary>
        /// 分页获取表格
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="select"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public async Task<Tuple<DataTable, int>> GetTable(Expression<Func<T, bool>> expression, string[] select, int pageSize, int pageIndex)
        {
            string selectStr = string.Join(",", select);
            return await Task.Run(() =>
            {
                int total = 0;
                var q = Db.Queryable<T>()
                    .WhereIF(expression != null, expression)
                    .Select(selectStr).ToDataTablePage(pageIndex, pageSize, ref total);
                return new Tuple<DataTable, int>(q, total);

            });

        }

        /// <summary>
        /// 获取数据表格
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="select"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortAsc"></param>
        /// <returns></returns>
        public async Task<DataTable> GetTable(Expression<Func<T, bool>> expression, string where, string[] select, Expression<Func<T, object>> sortExpression, bool sortAsc = true)
        {
            string selectStr = string.Join(",", select);
            OrderByType orderBy = sortAsc ? OrderByType.Asc : OrderByType.Desc;
            return await Task.Run(() =>
            {
                var q = Db.Queryable<T>()
                .WhereIF(expression != null, expression)
                .WhereIF(!string.IsNullOrEmpty(where), where)
                .OrderByIF(sortExpression != null, sortExpression, orderBy)
                .Select(selectStr).ToDataTable();
                return q;
            });
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortAsc"></param>
        /// <returns></returns>
        public async Task<Tuple<List<T>, int>> List(Expression<Func<T, bool>> expression, string where, int pageSize, int pageIndex, Expression<Func<T, object>> sortExpression, bool sortAsc = true)
        {
            OrderByType orderBy = sortAsc ? OrderByType.Asc : OrderByType.Desc;

            return await Task.Run(() =>
            {
                int total = 0;
                var q = Db.Queryable<T>()
                    .WhereIF(expression != null, expression)
                    .WhereIF(!string.IsNullOrEmpty(where), where)
                    .OrderByIF(sortExpression != null, sortExpression, orderBy)
                    .ToPageList(pageIndex, pageSize, ref total);

                return new Tuple<List<T>, int>(q, total);
            });
        }

        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<List<T>> List(Expression<Func<T, bool>> expression)
        {
            return await Task.Run(() => Db
                .Queryable<T>()
                .WhereIF(expression != null, expression)
                .ToList()
            );
        }

        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortAsc"></param>
        /// <returns></returns>
        public async Task<List<T>> List(Expression<Func<T, bool>> expression, string where = null, Expression<Func<T, object>> sortExpression = null, bool sortAsc = true)
        {
            OrderByType orderBy = sortAsc ? OrderByType.Asc : OrderByType.Desc;
            return await Task.Run(() =>
            {

                var q = Db.Queryable<T>()
                    .WhereIF(expression != null, expression)
                    .WhereIF(!string.IsNullOrEmpty(where), where)
                    .OrderByIF(sortExpression != null, sortExpression, orderBy)
                    .ToList();
                return new List<T>(q);
            });
        }

        /// <summary>
        /// 分页获取数据表格
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="select"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortAsc"></param>
        /// <returns></returns>
        public async Task<Tuple<DataTable, int>> GetTable(Expression<Func<T, bool>> expression, string where, string[] select, int pageSize, int pageIndex, Expression<Func<T, object>> sortExpression, bool sortAsc = true)
        {
            SqlSugar.OrderByType orderBy = sortAsc ? OrderByType.Asc : OrderByType.Desc;
            string selectStr = string.Join(",", select);
            return await Task.Run(() =>
            {
                int total = 0;
                var q = Db.Queryable<T>()
                    .WhereIF(expression != null, expression)
                    .WhereIF(!string.IsNullOrEmpty(where), where)
                    .OrderByIF(sortExpression != null, sortExpression, orderBy)
                    .Select(selectStr)
                    .ToDataTablePage(pageIndex, pageSize, ref total);
                return new Tuple<DataTable, int>(q, total);
            });
        }

        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public async Task<Tuple<List<T>, int>> List(Expression<Func<T, bool>> expression, int pageSize, int pageIndex)
        {
            return await Task.Run(() =>
            {
                int total = 0;
                var q = Db.Queryable<T>()
                    .WhereIF(expression != null, expression)
                    .ToPageList(pageIndex, pageSize, ref total);
                return new Tuple<List<T>, int>(q, total);
            }
           );
        }

        public async Task<Tuple<List<T>, int>> List(Expression<Func<T, bool>> expression, string where, int pageSize, int pageIndex)
        {
            return await Task.Run(() =>
            {
                int total = 0;
                var q = Db.Queryable<T>()
                    .WhereIF(expression != null, expression)
                    .WhereIF(!string.IsNullOrEmpty(where), where)
                    .ToPageList(pageIndex, pageSize, ref total);
                return new Tuple<List<T>, int>(q, total);
            }
           );
        }

        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <param name="exp1"></param>
        /// <param name="exp2"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public async Task<Tuple<List<T>, int>> List(Expression<Func<T, bool>> exp1, Expression<Func<T, bool>> exp2, int pageSize, int pageIndex, Expression<Func<T, object>> sortExpression = null, bool sortAsc = true)
        {
            OrderByType orderBy = sortAsc ? OrderByType.Asc : OrderByType.Desc;
            return await Task.Run(() =>
            {
                int total = 0;
                var q = Db.Queryable<T>()
                    .WhereIF(exp1 != null, exp1)
                    .WhereIF(exp2 != null, exp2)
                    .OrderByIF(sortExpression != null, sortExpression, orderBy)
                    .ToPageList(pageIndex, pageSize, ref total);
                return new Tuple<List<T>, int>(q, total);
            }
           );
        }

        /// <summary>
        /// 是否存在相关的数据
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public async Task<bool> IsExist(Expression<Func<T, bool>> expression)
        {
            return await Task.Run(() =>
            {
                var q = Db.Queryable<T>()
               .WhereIF(expression != null, expression)
               .Any();
                return q;
            });
        }
        public async Task<bool> IsExist(Expression<Func<T, bool>> expression, string where)
        {
            return await Task.Run(() =>
            {
                var q = Db.Queryable<T>()
               .WhereIF(expression != null, expression)
               .WhereIF(!string.IsNullOrEmpty(where), where)
               .Any();
                return q;
            });
        }

        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="Sort"></param>
        /// <returns></returns>
        public async Task<Tuple<List<T>, int>> List(Expression<Func<T, bool>> expression, string where, int pageSize, int pageIndex, string Sort)
        {
            return await Task.Run(() =>
            {
                int total = 0;
                var q = Db.Queryable<T>()
                    .WhereIF(expression != null, expression)
                    .WhereIF(!string.IsNullOrEmpty(where), where)
                    .OrderByIF(!string.IsNullOrEmpty(Sort), Sort)
                    .ToPageList(pageIndex, pageSize, ref total);
                return new Tuple<List<T>, int>(q, total);
            });
        }

        /// <summary>
        /// 获取数据列表
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="Sort"></param>
        /// <returns></returns>
        public async Task<Tuple<List<T>, int>> List(Expression<Func<T, bool>> expression, Expression<Func<T, bool>> expression1, int pageSize, int pageIndex, string Sort)
        {
            return await Task.Run(() =>
            {
                int total = 0;
                var q = Db.Queryable<T>()
                    .WhereIF(expression != null, expression)
                    .WhereIF(expression1 != null, expression1)
                    .OrderByIF(!string.IsNullOrEmpty(Sort), Sort)
                    .ToPageList(pageIndex, pageSize, ref total);
                return new Tuple<List<T>, int>(q, total);
            });
        }

        /// <summary>
        /// 获取数据表格
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="select"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="Sort"></param>
        /// <param name="Excel"></param>
        /// <returns></returns>
        public async Task<Tuple<DataTable, int>> GetTable(Expression<Func<T, bool>> expression, string where, string[] select, int pageSize, int pageIndex,
            string Sort, bool Excel = false)
        {
            string selectStr = string.Join(",", select);
            return await Task.Run(() =>
            {
                int total = 0;
                /*Excel模式下不需要每次查看total,为了减少TOTAL计算*/
                if (Excel)
                {
                    var q = Db.Queryable<T>()
                        .WhereIF(expression != null, expression)
                        .WhereIF(!string.IsNullOrEmpty(where), where)
                        .OrderByIF(!string.IsNullOrEmpty(Sort), Sort)
                        .Select(selectStr).ToDataTablePage(pageIndex, pageSize);
                    return new Tuple<DataTable, int>(q, total);
                }
                else
                {
                    var q = Db.Queryable<T>()
                       .WhereIF(expression != null, expression)
                       .WhereIF(!string.IsNullOrEmpty(where), where)
                       .OrderByIF(!string.IsNullOrEmpty(Sort), Sort)
                       .Select(selectStr).ToDataTablePage(pageIndex, pageSize, ref total);
                    return new Tuple<DataTable, int>(q, total);
                }
            });
        }

        /// <summary>
        /// 获取数据表格
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="select"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="Sort"></param>
        /// <param name="Excel"></param>
        /// <returns></returns>
        public virtual async Task<Tuple<List<T>, int>> GetList(Expression<Func<T, bool>> expression, string where, int pageIndex, int pageSize,
           string Sort, bool Excel = false)
        {

            return await Task.Run(() =>
            {
                int total = 0;
                /*Excel模式下不需要每次查看total,为了减少TOTAL计算*/
                if (Excel)
                {
                    var q = Db.Queryable<T>()
                        .WhereIF(expression != null, expression)
                        .WhereIF(!string.IsNullOrEmpty(where), where)
                        .OrderByIF(!string.IsNullOrEmpty(Sort), Sort)
                         .ToPageList(pageIndex, pageSize);
                    return new Tuple<List<T>, int>(q, total);
                }
                else
                {
                    var q = Db.Queryable<T>()
                       .WhereIF(expression != null, expression)
                       .WhereIF(!string.IsNullOrEmpty(where), where)
                       .OrderByIF(!string.IsNullOrEmpty(Sort), Sort)
                       .ToPageList(pageIndex, pageSize, ref total);
                    return new Tuple<List<T>, int>(q, total);
                }
            });
        }

        /// <summary>
        /// 返回带权限的表格
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="fieldexp"></param>
        /// <param name="sortExpression"></param>
        /// <param name="client"></param>
        /// <param name="sortAsc"></param>
        /// <returns></returns>
        public async Task<DataTable> GetDataScopeTable(Expression<Func<T, bool>> expression, string where, Expression<Func<T, object>> fieldexp, Expression<Func<T, object>> sortExpression, ClientInformation client, bool sortAsc = true)
        {
            OrderByType orderBy = sortAsc ? OrderByType.Asc : OrderByType.Desc;
            return await Task.Run(() =>
            {
                var q = Db.Queryable<T>()
                    .WhereIF(expression != null, expression)
                    .WhereIF(!string.IsNullOrEmpty(where), where)
                    .GroupBy(fieldexp)
                    .OrderByIF(sortExpression != null, sortExpression, orderBy)
                    .AddParameters(new
                    {
                        api_psnid = client.UserID
                    })
                    .Select(fieldexp).ToDataTable();
                return q;
            });
        }

        /// <summary>
        /// 分页返回带权限的表格
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="select"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="Sort"></param>
        /// <param name="client"></param>
        /// <param name="Excel"></param>
        /// <returns></returns>
        public async Task<Tuple<DataTable, int>> GetDataScopeTable(Expression<Func<T, bool>> expression, string where, string[] select, int pageSize, int pageIndex, string Sort, ClientInformation client, bool Excel = false)
        {
            string selectStr = string.Join(",", select);
            return await Task.Run(() =>
            {
                int total = 0;
                var q = Db.Queryable<T>()
                    .WhereIF(expression != null, expression)
                    .WhereIF(!string.IsNullOrEmpty(where), where)
                    .OrderByIF(!string.IsNullOrEmpty(Sort), Sort)
                    .AddParameters(new
                    {
                        api_psnid = client.UserID
                    })
                    .Select(selectStr);
                if (Excel)
                {
                    var res = q.ToDataTablePage(pageIndex, pageSize);
                    return new Tuple<DataTable, int>(res, total);
                }
                else
                {
                    var res = q.ToDataTablePage(pageIndex, pageSize, ref total);
                    return new Tuple<DataTable, int>(res, total);
                }
            });
        }

        /// <summary>
        /// 分组获取数据
        /// </summary>
        /// <param name="where"></param>
        /// <param name="fieldexp"></param>
        /// <returns></returns>
        public async Task<object[]> GroupBy(string where, Expression<Func<T, object>> fieldexp)
        {
            return await Task.Run(() => Db.Queryable<T>()
                .WhereIF(!string.IsNullOrEmpty(where), where)
                .GroupBy(fieldexp)
                .Select(fieldexp)
                .ToList()
                .ToArray());
        }

        /// <summary>
        /// 分组获取数据
        /// </summary>
        /// <typeparam name="Entity"></typeparam>
        /// <param name="where"></param>
        /// <param name="fieldexp"></param>
        /// <returns></returns>
        public async Task<object[]> GroupBy<Entity>(string where, Expression<Func<Entity, object>> fieldexp)
        {
            return await Task.Run(() => Db.Queryable<Entity>()
                .WhereIF(!string.IsNullOrEmpty(where), where)
                .GroupBy(fieldexp).Select(fieldexp).ToList().ToArray());
        }

        /// <summary>
        /// 分组获取数据
        /// </summary>
        /// <param name="exp"></param>
        /// <param name="fieldexp"></param>
        /// <returns></returns>
        public async Task<object[]> GroupBy(Expression<Func<T, bool>> exp, Expression<Func<T, object>> fieldexp)
        {
            return await Task.Run(() => Db.Queryable<T>()
               .WhereIF(exp != null, exp)
               .GroupBy(fieldexp).Select(fieldexp).ToList().ToArray());
        }

        /// <summary>
        /// 分组获取数据
        /// </summary>
        /// <param name="exp"></param>
        /// <param name="fieldexp"></param>
        /// <returns></returns>
        public async Task<object> Sum(Expression<Func<T, bool>> exp, Expression<Func<T, object>> fieldexp)
        {
            return await Task.Run(() => Db.Queryable<T>()
                  .WhereIF(exp != null, exp).Sum(fieldexp));
        }

        public async Task<object> Sum(Expression<Func<T, bool>> exp, Expression<Func<T, object>> fieldexp, string where)
        {
            return await Task.Run(() => Db.Queryable<T>()
                  .WhereIF(exp != null, exp)
                  .WhereIF(!string.IsNullOrEmpty(where), where)
                  .Sum(fieldexp));
        }
    }
}
