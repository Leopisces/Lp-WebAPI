﻿using Lp.LogAnalysis;
using Microsoft.Extensions.Options;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Lp.SqlSugarORM
{
    /// <summary>
    /// 基础类
    /// </summary>
    public abstract class BaseRepository
    {
        public SqlSugarClient Db
        {
            get
            {
                return DBClient();
            }
        }
       
        //protected Stopwatch _stopwatch;

        protected readonly SugarOption sugarOption;
        protected SqlDatabaseEnum DatabaseEnum { get; set; }

        //protected readonly ILogClient _log;
        protected abstract void init();

        public string ConnectionString
        {
            get
            {
                string _sqlConType_name = DatabaseEnum.ToString().ToLower();
                var obj = sugarOption.Databases.Where(c => c.DBName.ToLower() == _sqlConType_name).FirstOrDefault();
                if (obj == null)
                    return null;
                else
                    return obj.Master;
            }
        }

        public string DbType
        {
            get
            {
                string _sqlConType_name = DatabaseEnum.ToString().ToLower();
                var obj = sugarOption.Databases.Where(c => c.DBName.ToLower() == _sqlConType_name).FirstOrDefault();
                if (obj == null)
                    return null;
                else
                    return obj.DbType;
            }
        }


        /// <summary>
        /// 初始化
        /// </summary>
        public BaseRepository(IOptions<SugarOption> options)
        {
            this.sugarOption = options.Value;
            //this._stopwatch = new Stopwatch();

            init();
        }
        protected SqlSugarClient DBClient(bool AutoClose=true)
        {
            //拼装
            string _sqlConType_name = DatabaseEnum.ToString().ToLower();
            var obj = sugarOption.Databases.Where(c => c.DBName.ToLower() == _sqlConType_name).FirstOrDefault();
            //Console.WriteLine(obj.Master);
            if (obj == null)
            {
                //_logger.Fatal("not found sugar configure node in appsettiing.json");
                //_log.WriteFatal("not found sugar configure node in appsettiing.json");
                throw new Exception("not found sugar configure node in appsettiing.json");
            }

            try
            {
                var dbtype = (obj.DbType.ToLower()) switch
                {
                    "mysql" => SqlSugar.DbType.MySql,
                    "mssql" => SqlSugar.DbType.SqlServer,
                    _ => throw new Exception("sugarsql type error , appsetting.json"),
                };
                SqlSugarClient db = new SqlSugarClient(
                    new ConnectionConfig()
                    {
                        ConnectionString = obj.Master,
                        DbType = dbtype,//设置数据库类型
                        IsAutoCloseConnection = AutoClose,//自动释放数据务，如果存在事务，在事务结束后释放
                        InitKeyType = InitKeyType.Attribute, //从实体特性中读取主键自增列信息
                        ConfigureExternalServices = new ConfigureExternalServices()
                        {
                            SqlFuncServices = ExpMethods(dbtype)   //扩展方法
                        },
                       
                         
                    }
                );
                db.Ado.CommandTimeOut = 20000;//设置超时时间
                
               

                //创建SLAVE
                if (obj.Slaves != null && obj.Slaves.Count() > 0)
                {
                    db.CurrentConnectionConfig.SlaveConnectionConfigs = new List<SlaveConnectionConfig>();
                    //int hitRate = 100 / obj.Slaves.Count();
                    foreach (var slave in obj.Slaves)
                    {
                        Console.WriteLine(slave);
                        db.CurrentConnectionConfig.SlaveConnectionConfigs.Add(new SlaveConnectionConfig() { ConnectionString = slave, HitRate = 30  });
                    }
                }

                //用来打印Sql方便你调式    
                db.Aop.OnLogExecuting = (sql, pars) =>
                {
                    //_stopwatch.Start();
                    //Console.WriteLine(db.CurrentConnectionConfig.ConnectionString);
                    //Console.WriteLine(sql + "OnLogExecuting:\r\n" + db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
                    //_log.WriteSQL("OnLogExecuting",
                    //    Db.Ado.Connection.ConnectionString + "\r\n" + sql + "\r\n" + Db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value))
                    //    , "sys");
                };
                db.Aop.OnLogExecuted = (sql, pars) =>
                {
                    //_stopwatch.Stop();
                    Console.WriteLine(sql + "OnLogExecuted:\r\n" +db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value)));
                    //Console.WriteLine();

                    //_log.WriteSQL("OnLogExecuted",
                    //        Db.Ado.Connection.ConnectionString + "\r\n" + sql + "\r\n" + Db.Utilities.SerializeObject(pars.ToDictionary(it => it.ParameterName, it => it.Value))
                    //        , "sys", new { ElapsedMilliseconds = _stopwatch.ElapsedMilliseconds });
                };
                db.Aop.OnError = ex =>
                {
                   // _stopwatch.Stop();
                    Console.WriteLine(ex.Message);
                    Console.WriteLine();
                    //_log.WriteError("OnError", ex);

                };
                
                return db;
            }
            catch (Exception ex)
            {

                //_log.WriteError("ex", ex);
                throw ex;
            }
        }

        /// <summary>
        /// 扩展Lambda
        /// </summary>
        /// <param name="dbtype"></param>
        /// <returns></returns>
        private List<SqlFuncExternal> ExpMethods(SqlSugar.DbType dbtype)
        {
            var expMethods = new List<SqlFuncExternal>();
            expMethods.Add(new SqlFuncExternal()
            {
                UniqueMethodName = "Round",
                MethodValue = (expInfo, dbType, expContext) =>
                {
                    if (dbtype == SqlSugar.DbType.MySql)
                        return string.Format("Round({0},{1})", expInfo.Args[0].MemberName, expInfo.Args[1].MemberName);
                    else
                        throw new Exception("未实现");
                }
            });
            return expMethods;
        }

        /// <summary>
        /// 四舍五入保留i位数
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public static decimal? Round<T>(T str, int i)
        {
            throw new NotSupportedException("Can only be used in expressions");
        }
    }
}
