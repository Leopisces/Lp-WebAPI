﻿using Lp.Core.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Lp.SqlSugarORM
{
    public partial interface IBaseDataRepository<T> where T : class, new()
    {
        /// <summary>
        /// 返回列表
        /// </summary>
        Task<Tuple<List<T>, int>> List(Expression<Func<T, bool>> expression, int pageSize, int pageIndex);

        Task<Tuple<List<T>, int>> List(Expression<Func<T, bool>> expression,string where, int pageSize, int pageIndex);

        Task<Tuple<List<T>, int>> List(Expression<Func<T, bool>> exp1, Expression<Func<T, bool>> exp2, int pageSize, int pageIndex, Expression<Func<T, object>> sortExpression = null, bool sortAsc = true);

        Task<List<T>> List(Expression<Func<T, bool>> expression);

        Task<List<T>> List(Expression<Func<T, bool>> expression, string where = null, Expression<Func<T, object>> sortExpression = null, bool sortAsc = true);

        Task<object> GetKey(T obj, IClientInformation client);
        /// <summary>
        /// 取单个
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<T> GetSingle(Expression<Func<T, bool>> expression);

        /// <summary>
        /// 返回列表
        /// </summary>
        Task<Tuple<List<T>, int>> List(Expression<Func<T, bool>> expression,
                                       string where,
                                       int pageSize,
                                       int pageIndex,
                                       Expression<Func<T, object>> sortExpression,
                                       bool sortAsc = true);
        /// <summary>
        /// 返回列表
        /// </summary>
        Task<Tuple<List<T>, int>> List(Expression<Func<T, bool>> expression,
                               string where,
                               int pageSize,
                               int pageIndex,
                               string Sort);
        /// <summary>
        /// 返回列表
        /// </summary>
        Task<Tuple<List<T>, int>> List(Expression<Func<T, bool>> expression,
                               Expression<Func<T, bool>> expression1,
                               int pageSize,
                               int pageIndex,
                               string Sort);
        /// <summary>
        /// 返回单个
        /// </summary>
        Task<T> Get(object key, IClientInformation client);

        Task<T> Get(object key);

        /// <summary>
        /// 返回总行数
        /// </summary>
        Task<int> Count(Expression<Func<T, bool>> expression, string where);

        /// <summary>
        /// 返回表
        /// </summary>
        Task<Tuple<DataTable, int>> GetTable(string[] select, int pageSize, int pageIndex);

        /// <summary>
        /// 返回表(不分页)
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="select"></param>
        /// <returns></returns>
        Task<DataTable> GetTable(Expression<Func<T, bool>> expression, string where, string[] select, Expression<Func<T, object>> sortExpression, bool sortAsc = true);

        Task<Tuple<DataTable, int>> GetTable(Expression<Func<T, bool>> expression, string[] select, int pageSize, int pageIndex);

        Task<DataTable> GetTable(Expression<Func<T, bool>> expression,string[] select=null);

        /// <summary>
        /// 返回带权限的数据表
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="fieldexp"></param>
        /// <param name="sortExpression"></param>
        /// <param name="client"></param>
        /// <param name="sortAsc"></param>
        /// <returns></returns>
        Task<DataTable> GetDataScopeTable(Expression<Func<T, bool>> expression, string where, Expression<Func<T, object>> fieldexp, Expression<Func<T, object>> sortExpression, ClientInformation client, bool sortAsc = true);

        /// <summary>
        /// 分页返回带权限的数据表
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="select"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="Sort"></param>
        /// <param name="client"></param>
        /// <param name="Excel"></param>
        /// <returns></returns>
        Task<Tuple<DataTable, int>> GetDataScopeTable(Expression<Func<T, bool>> expression, string where, string[] select, int pageSize, int pageIndex, string Sort, ClientInformation client, bool Excel = false);
        Task<Tuple<List<T>, int>> GetList(Expression<Func<T, bool>> expression, string where,  int pageIndex,  int pageSize,
           string Sort, bool Excel = false);
        /// <summary>
        /// 返回表
        /// </summary>
        Task<Tuple<DataTable, int>> GetTable(Expression<Func<T, bool>> expression,
                                       string where,
                                       string[] select,
                                       int pageSize,
                                       int pageIndex,
                                       Expression<Func<T, object>> sortExpression,
                                       bool sortAsc = true);
        /// <summary>
        /// 返回表
        /// </summary>
        Task<Tuple<DataTable, int>> GetTable(Expression<Func<T, bool>> expression,
                               string where,
                               string[] select,
                               int pageSize,
                               int pageIndex,
                               string Sort,
                               bool Excel = false

            );

        /// <summary>
        /// 是否存在相关的数据
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<bool> IsExist(Expression<Func<T, bool>> expression);

        Task<bool> IsExist(Expression<Func<T, bool>> expression, string where);
        Task<object[]> GroupBy(string where, Expression<Func<T, object>> fieldexp);

        Task<object[]> GroupBy(Expression<Func<T, bool>> exp, Expression<Func<T, object>> fieldexp);


        Task<object[]> GroupBy<Entity>(string where, Expression<Func<Entity, object>> fieldexp);


        Task<object> Sum(Expression<Func<T, bool>> exp, Expression<Func<T, object>> fieldexp );
        Task<object> Sum(Expression<Func<T, bool>> exp, Expression<Func<T, object>> fieldexp ,string where );
        /// <summary>
        /// input object and find primarykey, then find then dest object by primarykey
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        Task<T> GetObjectByOldObject(T obj);

        Task<object> GetPrimaryObj(T obj);

        Task<List<T>> ListByPrimaryArr(object[] arr);
    }

    /// <summary>
    /// 数据查询接口底层实现
    ///   2张表关联
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    public partial interface IBaseDataRepository<T1, T2,T> where T1 : class, new() where T2 : class, new()
    {
        /// <summary>
        /// 返回表
        /// </summary>
        /// <param name="join"></param>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="select"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="Sort"></param>
        /// <param name="Excel"></param>
        /// <returns></returns>
        Task<Tuple<DataTable, int>> GetTable(Expression<Func<T1, T2, bool>> join, Expression<Func<T, bool>> expression, string where, string[] select, int pageSize, int pageIndex, string Sort, bool Excel = false);

        /// <summary>
        /// 返回总行数
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<int> Count(Expression<Func<T1, T2, bool>> join,Expression<Func<T, bool>> expression, string where);

        /// <summary>
        /// 分页获取列表
        /// </summary>
        /// <param name="join"></param>
        /// <param name="expression"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        Task<Tuple<List<T>, int>> List(Expression<Func<T1, T2, bool>> join, Expression<Func<T, bool>> expression, int pageSize, int pageIndex);
    }

    /// <summary>
    /// 数据查询接口底层实现
    ///   3张表关联
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="T3"></typeparam>
    public partial interface IBaseDataRepository<T1, T2,T3,T> where T1 : class, new() where T2 : class, new() where T3 : class, new() where T : class, new()
    {
        /// <summary>
        /// 返回表
        /// </summary>
        /// <param name="join"></param>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="select"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="Sort"></param>
        /// <param name="Excel"></param>
        /// <returns></returns>
        Task<Tuple<DataTable, int>> GetTable(Expression<Func<T1, T2,T3, bool>> join, Expression<Func<T, bool>> expression, string where, string[] select, int pageSize, int pageIndex, string Sort, bool Excel = false);

        /// <summary>
        /// 返回总行数
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<int> Count(Expression<Func<T1, T2,T3, bool>> join, Expression<Func<T, bool>> expression, string where);
    }

    /// <summary>
    /// 数据查询接口底层实现
    ///   4张表关联
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="T3"></typeparam>
    /// <typeparam name="T4"></typeparam>
    public partial interface IBaseDataRepository<T1, T2, T3,T4, T> where T1 : class, new() where T2 : class, new() where T3 : class, new() where T : class, new()
    {
        /// <summary>
        /// 返回表
        /// </summary>
        /// <param name="join"></param>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <param name="select"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="Sort"></param>
        /// <param name="Excel"></param>
        /// <returns></returns>
        Task<Tuple<DataTable, int>> GetTable(Expression<Func<T1, T2, T3,T4, bool>> join, Expression<Func<T, bool>> expression, string where, string[] select, int pageSize, int pageIndex, string Sort, bool Excel = false);

        /// <summary>
        /// 返回总行数
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        Task<int> Count(Expression<Func<T1, T2, T3,T4, bool>> join, Expression<Func<T, bool>> expression, string where);
    }
}
