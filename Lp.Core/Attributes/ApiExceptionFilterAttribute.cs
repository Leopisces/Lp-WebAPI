﻿using Lp.LogAnalysis;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lp.Core.Attributes
{
    /// <summary>
    /// 表示处理Api异常的筛选器.
    /// </summary>
    public class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        //private readonly ILogClient _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiExceptionFilterAttribute" /> class.
        /// </summary>
        /// <param name="logger">The logger</param>
        public ApiExceptionFilterAttribute(
            //ILogClient logger
        )
        {
            //_logger = logger;
        }

        /// <summary>
        /// Called when [exception].
        /// </summary>
        /// <param name="context">The context.</param>
        public override void OnException(ExceptionContext context)
        {

            //if (context.Exception is TokenException)
            //{
            //    var ex = context.Exception as TokenException;
            //    _logger.WriteError($"AspNetCore.OnTokenException.SqlException:ip={context.HttpContext.Connection.RemoteIpAddress}, path={context.HttpContext.Request.Path}", ex);
            //    context.Result = new JsonResult(new ResponseResult { code = APIStatusCode.TokenFailure, message = ex.Message });

            //}
            //else if (context.Exception is SqlException)
            //{
            //    var ex = context.Exception as SqlException;
            //    _logger.WriteError($"AspNetCore.OnException.SqlException:ip={context.HttpContext.Connection.RemoteIpAddress}, path={context.HttpContext.Request.Path}", ex);
            //    context.Result = new JsonResult(ResponseResult.Error("SQL错误：[" + ex.Message + "]"));
            //}
            //else if (context.Exception is SqlSugar.SqlSugarException)
            //{
            //    //由Sugar返回的错误
            //    var ex = context.Exception as SqlSugar.SqlSugarException;
            //    _logger.WriteError($"AspNetCore.OnException.SqlSugarException:ip={context.HttpContext.Connection.RemoteIpAddress}, path={context.HttpContext.Request.Path}", ex);
            //    context.Result = new JsonResult(ResponseResult.Error("SQLSUGAR错误：[" + ex.Sql + "]"));
            //}
            //else
            //{
            //    _logger.WriteError($"AspNetCore.OnException.Other:ip={context.HttpContext.Connection.RemoteIpAddress}, path={context.HttpContext.Request.Path}", context.Exception);
            //    context.Result = new ObjectResult(ResponseResult.Error(context.Exception.Message));
            //}

            context.ExceptionHandled = true;
        }
    }
}
