﻿namespace Lp.Core.Entity
{
    public interface IClientInformation
    {
        /// <summary>
        /// 系统编码
        /// </summary>
        int UserID { get; set; }
        /// <summary>
        /// 用户工号
        /// </summary>
        string Code { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        string RealName { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        string Mobile { get; set; }
    }
}
