﻿namespace Lp.Core.Entity
{
    /// <summary>
    /// 存入REDIS结构
    /// </summary>
    public class ClientInformation : IClientInformation
    {
        /// <summary>
        /// 系统编码
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// 用户工号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string Nick_Name { get; set; }

        /// <summary>
        /// 登录方式
        /// </summary>
        public string PlatForm { get; set; }

        /// <summary>
        /// 是否是手机端
        /// </summary>
        public int IsMobile { get; set; }

        /// <summary>
        /// 真实姓名拼音
        /// </summary>
        public string Spell { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// 电子邮件
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 是否管理员
        /// </summary>
        public bool IsAdmin { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string Account { get; set; }
    }
}