﻿using Lp.Core.Enum;
using Lp.Core.Helper;
using MessagePack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lp.Core.Entity
{
    /// <summary>
    /// 响应结果类
    /// </summary>
    [MessagePackObject(true)]
    public class ResponseResult
    {
        /// <summary>
        /// 状态(0-default, 1-success, -1-error)
        /// </summary>
        public APIStatusCode code
        {
            get;
            set;
        }

        /// <summary>
        /// 消息内容
        /// </summary>
        public string message
        {
            get;
            set;
        }
        /// <summary>
        /// 数据
        /// </summary>
        public object data
        {
            get;
            set;
        }

        /// <summary>
        /// 响应消息封装类
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ResponseResult Default(string message = null)
        {
            var result = new ResponseResult();
            result.code = APIStatusCode.Unknown;
            result.message = message;

            return result;
        }
        /// <summary>
        /// 响应消息封装类
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ResponseResult Success(string message = null)
        {
            var result = new ResponseResult();
            result.code = APIStatusCode.Success;
            result.message = MessageHelper.OKMESSAGE + " " + message;

            return result;
        }
        ///响应消息封装类
        public static ResponseResult Success(object data, string message = null)
        {
            var result = new ResponseResult();
            result.code = APIStatusCode.Success;
            result.message = MessageHelper.OKMESSAGE + " " + message;
            result.data = data;

            return result;
        }
        ///响应消息封装类
        public static ResponseResult Success(object data, int count, int pageIndex, int pageSize, string message = null)
        {
            var result = new ResponseResult();
            result.code = APIStatusCode.Success;
            result.message = MessageHelper.OKMESSAGE + " " + message;
            result.data = new
            {
                list = data,
                total = count,
                pageindex = pageIndex,
                pagesize = pageSize
            };


            return result;
        }

        public static ResponseResult Success(object data, long count, int pageIndex, int pageSize, string message = null)
        {
            var result = new ResponseResult();
            result.code = APIStatusCode.Success;
            result.message = MessageHelper.OKMESSAGE + " " + message;
            result.data = new
            {
                list = data,
                total = count,
                pageindex = pageIndex,
                pagesize = pageSize
            };


            return result;
        }

        /// <summary>
        /// 响应消息封装类
        /// </summary>
        /// <returns></returns>
        public static ResponseResult Error(string errorMessage = null)
        {
            var result = new ResponseResult();
            result.code = APIStatusCode.Failed;
            result.message = MessageHelper.ERRORMESSAGE + " " + errorMessage;
            return result;
        }

        /// <summary>
        /// Http 响应消息封装类
        /// </summary>
        /// <param name="message"></param>
        /// <param name="SuccessCode"></param>
        /// <returns></returns>
        public static ResponseResult Error(APIStatusCode SuccessCode, string message = null)
        {
            var result = new ResponseResult();
            result.data = null;
            result.code = SuccessCode;
            result.message = MessageHelper.ERRORMESSAGE + " " + message;

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ResponseResult Deserialize(string message)
        {
            var result = JsonConvert.DeserializeObject<ResponseResult>(message);

            return result;
        }
        public static ResponseResult Judge(bool b, string message = null)
        {
            if (b)
                return ResponseResult.Success(message);
            else
                return ResponseResult.Error(message);

        }
    }

    /// <summary>
    /// 响应结果类
    /// </summary>
    [MessagePackObject(true)]
    public class ResponseResult<T> where T : class, new()
    {
        /// <summary>
        /// 状态(0-default, 1-success, -1-error)
        /// </summary>
        public APIStatusCode code
        {
            get;
            set;
        }

        /// <summary>
        /// 消息内容
        /// </summary>
        public string message
        {
            get;
            set;
        }

        /// <summary>
        /// 数据
        /// </summary>
        public T data
        {
            get;
            set;
        }

        /// <summary>
        /// 响应消息封装类
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ResponseResult<T> Default(string message = null)
        {
            var result = new ResponseResult<T>();
            result.code = APIStatusCode.Unknown;
            result.message = message;

            return result;
        }
        /// <summary>
        /// 响应消息封装类
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ResponseResult<T> Success(string message = null)
        {
            var result = new ResponseResult<T>();
            result.code = APIStatusCode.Success;
            result.message = MessageHelper.OKMESSAGE + " " + message;

            return result;
        }
        ///响应消息封装类
        public static ResponseResult<T> Success(T data, string message = null)
        {
            var result = new ResponseResult<T>();
            result.code = APIStatusCode.Success;
            result.message = MessageHelper.OKMESSAGE + " " + message;
            result.data = data;

            return result;
        }

        /// <summary>
        /// 响应消息封装类
        /// </summary>
        /// <returns></returns>
        public static ResponseResult<T> Error(string errorMessage = null)
        {
            var result = new ResponseResult<T>();
            result.code = APIStatusCode.Failed;
            result.message = MessageHelper.ERRORMESSAGE + " " + errorMessage;
            return result;
        }

        /// <summary>
        /// Http 响应消息封装类
        /// </summary>
        /// <param name="message"></param>
        /// <param name="SuccessCode"></param>
        /// <returns></returns>
        public static ResponseResult<T> Error(APIStatusCode SuccessCode, string message = null)
        {
            var result = new ResponseResult<T>();
            result.data = null;
            result.code = SuccessCode;
            result.message = MessageHelper.ERRORMESSAGE + " " + message;

            return result;
        }
        ///
        public static ResponseResult<T> Deserialize(string message)
        {
            var result = JsonConvert.DeserializeObject<ResponseResult<T>>(message);

            return result;
        }
    }

    [MessagePackObject(true)]
    public class RData<T> where T : class, new()
    {
        public T list { get; set; }
        public long total { get; set; }
        public int pageindex { get; set; }
        public int pagesize { get; set; }
    }

    /// <summary>
    /// 响应结果类
    /// </summary>
    [MessagePackObject(true)]
    public class ResponseJudgeResult<T> where T : struct
    {
        /// <summary>
        /// 状态(0-default, 1-success, -1-error)
        /// </summary>
        public APIStatusCode code
        {
            get;
            set;
        }

        /// <summary>
        /// 消息内容
        /// </summary>
        public string message
        {
            get;
            set;
        }

        /// <summary>
        /// 数据
        /// </summary>
        public T data
        {
            get;
            set;
        }

        /// <summary>
        /// 响应消息封装类
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ResponseJudgeResult<T> Default(string message = null)
        {
            var result = new ResponseJudgeResult<T>();
            result.code = APIStatusCode.Unknown;
            result.message = message;

            return result;
        }
        /// <summary>
        /// 响应消息封装类
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ResponseJudgeResult<T> Success(string message = null)
        {
            var result = new ResponseJudgeResult<T>();
            result.code = APIStatusCode.Success;
            result.message = MessageHelper.OKMESSAGE + " " + message;

            return result;
        }
        ///响应消息封装类
        public static ResponseJudgeResult<T> Success(T data, string message = null)
        {
            var result = new ResponseJudgeResult<T>();
            result.code = APIStatusCode.Success;
            result.message = MessageHelper.OKMESSAGE + " " + message;
            result.data = data;

            return result;
        }

        /// <summary>
        /// 响应消息封装类
        /// </summary>
        /// <returns></returns>
        public static ResponseJudgeResult<T> Error(string errorMessage = null)
        {
            var result = new ResponseJudgeResult<T>();
            result.code = APIStatusCode.Failed;
            result.message = MessageHelper.ERRORMESSAGE + " " + errorMessage;
            return result;
        }

        /// <summary>
        /// Http 响应消息封装类
        /// </summary>
        /// <param name="message"></param>
        /// <param name="SuccessCode"></param>
        /// <returns></returns>
        public static ResponseJudgeResult<T> Error(APIStatusCode SuccessCode, string message = null)
        {
            var result = new ResponseJudgeResult<T>();
            result.data = default(T);
            result.code = SuccessCode;
            result.message = MessageHelper.ERRORMESSAGE + " " + message;

            return result;
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ResponseJudgeResult<T> Deserialize(string message)
        {
            var result = JsonConvert.DeserializeObject<ResponseJudgeResult<T>>(message);

            return result;
        }
    }
}
