﻿namespace Lp.Core.Entity
{
    /// <summary>
    /// 返回给前台结构
    /// </summary>
    public class LoginInfo
    {
        /// <summary>
        /// 状态
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// 身份令牌
        /// </summary>
        public string access_token { get; set; }

        /// <summary>
        /// 有效期(秒)
        /// </summary>
        public double expires_in { get; set; }

        /// <summary>
        /// 身份令牌类型
        /// </summary>
        public string token_type { get; set; }

        /// <summary>
        /// 基本信息
        /// </summary>
        public ClientInformation BasicInfo { get; set; }
    }
}