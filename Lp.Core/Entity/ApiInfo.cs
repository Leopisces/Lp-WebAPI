﻿using Microsoft.Extensions.Configuration;
using System.Reflection;

namespace Lp.Core.Entity
{
    public class ApiInfo : IApiInfo
    {
        private ApiInfo(IConfiguration config, Assembly assembly)
        {
            var info = config.GetSection("ApiInfo");
            Title = info["Title"];
            Version = info["Version"];
            ApiName = info["ApiName"];
            ApplicationAssembly = assembly;
        }
        public string Title { get; set; }
        public string ApiName { get; set; }
        public string BindAddress { get; set; }
        public string Version { get; set; }
        public int BindPort { get; set; }
        public Assembly ApplicationAssembly { get; set; }
        public static IApiInfo Instance { get; private set; }
        public static IApiInfo Instantiate(IConfiguration config, Assembly assembly)
        {
            Instance = new ApiInfo(config, assembly);
            return Instance;
        }
    }
}
