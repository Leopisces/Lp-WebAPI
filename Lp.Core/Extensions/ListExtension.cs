﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Lp.Core.Extensions
{
    public static class ListExtension
    {
        /// <summary>
        /// 返回赋值
        /// </summary>
        /// <typeparam name="D">目标</typeparam>
        /// <typeparam name="R">获取源</typeparam>
        /// <param name="list"></param>
        /// <param name="Prefunc">准备</param>
        /// <param name="getFunc">取数</param>
        /// <param name="method"></param>
        /// <returns></returns>
        public static List<D> FillData<D, R>(this List<D> list, Func<object[], Task<bool>> Prefunc, Func<object, R> getFunc, string method)
        {
            //D 目标属性
            var props = typeof(D).GetProperties();
            //主键属性
            PropertyInfo KeyProp = null;
            //目标字段 +　目标标签　
            List<Tuple<PropertyInfo, CollectionTranFieldAttribute>> TranFieldProps = new List<Tuple<PropertyInfo, CollectionTranFieldAttribute>>();
            //来源属性
            var Rprop = typeof(R).GetProperties();
            //遍历取主键  +　目标字段
            foreach (var p in props)
            {
                var attr = p.GetCustomAttribute<CollectionKeyFieldAttribute>();
                var attr2 = p.GetCustomAttribute<CollectionTranFieldAttribute>();
                if (attr != null && attr.MethodName == method)
                {
                    KeyProp = p;
                }
                if (attr2 != null && attr2.MethodName == method)
                {
                    TranFieldProps.Add(new Tuple<PropertyInfo, CollectionTranFieldAttribute>(p, attr2));
                }
            }
            //如果不存在，返回空
            if (KeyProp == null || TranFieldProps.Count == 0)
            {
                return null;
            }

            List<object> IdList = new List<object>();

            //取主键-值-数组 
            foreach (var node in list)
            {
                var id = KeyProp.GetValue(node);
                IdList.Add(id);
            }
            var IdArr = IdList.Distinct().ToArray();

            //通过PreFunc 预先加载数据至REDIS
            var List = Prefunc(IdArr).Result;
            //遍历列表，塞入对应数据
            foreach (var node in list)
            {
                object key = KeyProp.GetValue(node);//当前主键值
                R r = getFunc(key);//从REDIS取出单条相应记录

                if (r != null)
                {
                    //遍历 每个字段写入数据
                    foreach (var n in TranFieldProps)
                    {
                        var attr = n.Item2;
                        var prop = n.Item1;

                        var name = attr.SourceField ?? prop.Name;
                        var sourceProp = Rprop.Where(c => c.Name == name).FirstOrDefault();
                        if (sourceProp != null)
                        {
                            prop.SetValue(node, sourceProp.GetValue(r));
                        }
                    }

                }
            }
            return list;

        }

        /// <summary>
        /// 自定义Distinct扩展方法
        /// </summary>
        /// <typeparam name="T">要去重的对象类</typeparam>
        /// <typeparam name="C">自定义去重的字段类型</typeparam>
        /// <param name="source">要去重的对象</param>
        /// <param name="getfield">获取自定义去重字段的委托</param>
        /// <returns></returns>
        public static IEnumerable<T> MyDistinct<T, C>(this IEnumerable<T> source, Func<T, C> getfield)
        {
            return source.Distinct(new Compare<T, C>(getfield));
        }
    }
    public class CollectionTranFieldAttribute : Attribute
    {
        public CollectionTranFieldAttribute(string Method)
        {
            this.MethodName = Method;
        }
        public CollectionTranFieldAttribute(string Method, string Field)
        {
            this.SourceField = Field;
            this.MethodName = Method;
        }
        /// <summary>
        /// 替换模型中的字段属性名， 默认同名
        /// </summary>
        public string SourceField { get; set; }
        /// <summary>
        /// 方法分组 组名
        /// </summary>
        public string MethodName { get; set; }

    }

    public class CollectionKeyFieldAttribute : Attribute
    {
        public CollectionKeyFieldAttribute(string Method)
        {
            this.MethodName = Method;
        }
        /// <summary>
        /// 方法分组 组名
        /// </summary>
        public string MethodName { get; set; }
    }


    public class Compare<T, C> : IEqualityComparer<T>
    {
        private Func<T, C> _getField;
        public Compare(Func<T, C> getfield)
        {
            this._getField = getfield;
        }
        public bool Equals(T x, T y)
        {
            return EqualityComparer<C>.Default.Equals(_getField(x), _getField(y));
        }
        public int GetHashCode(T obj)
        {
            return EqualityComparer<C>.Default.GetHashCode(this._getField(obj));
        }
    }
}
