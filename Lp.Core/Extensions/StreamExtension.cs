﻿using System.IO;

namespace Lp.Core.Extensions
{
    public static class StreamExtension
    {
        public static byte[] ConvertToBytes(this Stream stream)
        {
            byte[] bytes = new byte[stream.Length];
            //设置当前流的位置为流的开始
            stream.Seek(0, SeekOrigin.Begin);
            stream.Read(bytes, 0, bytes.Length);

            return bytes;
        }
    }
}
