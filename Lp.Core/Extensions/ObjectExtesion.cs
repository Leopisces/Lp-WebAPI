﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lp.Core.Extensions
{
    public static class ObjectExtesion
    {
        public static string[] CollectionObject2String(this object[] objs)
        {
            var r = new List<string>();
            for (var i = 0; i < objs.Length; i++)
            {
                r.Add(objs[i].ToString());
            }
            return r.ToArray();
        }
    }
}
