﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lp.Core.Enum
{
    public enum APIStatusCode
    {
        /// <summary>
        /// 成功
        /// </summary>
        Success = 1,
        /// <summary>
        /// 重新登陆
        /// </summary>
        NeedRelogin = 0,
        /// <summary>
        /// Token验证失败
        /// </summary>
        Failed = -1,
        /// <summary>
        /// 未知码
        /// </summary>
        Unknown = 2,
        /// <summary>
        /// TOKEN 被占用
        /// </summary>
        TOKENOccupy = -2,
        /// <summary>
        /// Token失效
        /// </summary>
        TokenFailure = 401,
    }
}
