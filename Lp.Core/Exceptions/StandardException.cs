﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Lp.Core.Exceptions
{
    /// <summary>
    /// 标准的异常
    /// </summary>
    public class StandardException : Exception, IStandardError
    {
        private string _message;
        private List<IStandardError> _details = new List<IStandardError>();


        /// <summary>
        /// 标准的失败处理方法
        /// </summary>
        /// <param name="code">代码</param>
        /// <param name="message">消息</param>
        /// <param name="target">标签</param>
        /// <returns></returns>
        public static StandardException Caused(string code, string message, [CallerMemberName] string target = null) => new StandardException
        {
            Code = code,
            Message = message,
            Target = target,
        };

        /// <summary>
        /// code属性
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 消息属性
        /// </summary>
        public new string Message
        {
            get
            {
                if (string.IsNullOrEmpty(_message))
                {
                    return base.Message;
                }

                return _message;
            }
            set => _message = value;
        }

        /// <summary>
        /// 标签属性
        /// </summary>
        public string Target { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<IStandardError> Details
        {
            get => _details;
            set
            {
                if (value != null)
                {
                    _details.Clear();
                    _details.AddRange(value);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IStandardError InnerError { get; set; }

        /// <summary>
        /// 引发此异常
        /// </summary>
        public void Throw() => throw this;

        /// <summary>
        /// 添加错误信息
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        public StandardException Append(params IStandardError[] details)
        {
            if (details.Length > 0)
            {
                _details.AddRange(details);
            }

            // this for fluent API use
            // InvokeError.Caused(message).Append(details).Throw();
            return this;
        }

        /// <summary>
        /// 添加错误信息
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public StandardException Concat(Exception exception)
        {
            var error = new StandardException
            {
                Code = "excption",
                Message = exception.ToString(),
            };

            _details.Add(error);

            // this for fluent API use
            // InvokeError.Caused(message).Append(details).Throw();
            return this;
        }

        /// <summary>
        /// 添加错误信息
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        public StandardException Append(IEnumerable<IStandardError> details)
        {
            if (details != null)
            {
                _details.AddRange(details);
            }

            return this;
        }

        /// <summary>
        /// 重写tostring
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (string.IsNullOrEmpty(Code) || string.IsNullOrEmpty(_message))
            {
                return base.ToString();
            }

            if (_details.Count == 0)
            {
                return $"{"{ "}\"Code\": \"{Code}\", \"Message\": \"{Message}\", \"Target\": \"{Target}\"{" }"}";
            }
            else
            {
                return $"{"{ "}\"Code\": \"{Code}\", \"Message\": \"{Message}\", \"Details\": [{string.Join(",", _details)}]{" }"}";
            }
        }
    }
}
