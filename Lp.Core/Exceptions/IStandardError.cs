﻿using System.Collections.Generic;

namespace Lp.Core.Exceptions
{
    /// <summary>
    /// 标准的错误接口
    /// </summary>
    public interface IStandardError
    {
        /// <summary>
        /// 代码
        /// </summary>
        string Code { get; set; }
        /// <summary>
        /// 消息
        /// </summary>
        string Message { get; set; }
        /// <summary>
        /// 标签
        /// </summary>
        string Target { get; set; }
        /// <summary>
        /// 
        /// </summary>
        IStandardError InnerError { get; set; }
        /// <summary>
        /// 
        /// </summary>
        IEnumerable<IStandardError> Details { get; set; }
    }
}
