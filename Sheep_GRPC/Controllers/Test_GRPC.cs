﻿using Lp.SheepInterface;
using MagicOnion;
using MagicOnion.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sheep_GRPC.Controllers
{
    public class Test_GRPC : ServiceBase<ITest_GRPC>, ITest_GRPC
    {
        public async UnaryResult<int> Test()
        {
            return await Task.Run(() => 1);
        }
    }
}
