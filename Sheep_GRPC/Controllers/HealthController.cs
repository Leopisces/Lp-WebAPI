﻿using Microsoft.AspNetCore.Mvc;

namespace Sheep_GRPC.Controllers
{
    [Produces("application/json")]
    [Route("api/Sheep_GRPC/Health")]
    public class HealthController : Controller
    {
        [HttpGet]
        public IActionResult Get() => Ok("ok");
    }
}