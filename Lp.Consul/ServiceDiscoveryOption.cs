﻿
namespace Lp.Consul
{
    /// <summary>
    /// consul服务发现选项
    /// </summary>
    public class ConsulServiceDiscoveryOption
    {
        public string ServiceName { get; set; }

        public string Version { get; set; }

        /// <summary>
        /// consul注册配置
        /// </summary>
        public ConsulRegistryHostConfiguration Consul { get; set; }

        public string HealthCheckTemplate { get; set; }

        public string[] Endpoints { get; set; }
    }
}
