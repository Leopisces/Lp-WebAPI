﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Lp.Consul.Registry
{
    /// <summary>
    /// 查找服务接口
    /// </summary>
    public interface IResolveServiceInstances
    {
        /// <summary>
        /// 查找服务
        /// </summary>
        /// <returns></returns>
        Task<IList<RegistryInformation>> FindServiceInstancesAsync();
        /// <summary>
        /// 查找指定的服务
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<IList<RegistryInformation>> FindServiceInstancesAsync(string name);
        /// <summary>
        /// 根据版本号,服务名查找服务
        /// </summary>
        /// <param name="name"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        Task<IList<RegistryInformation>> FindServiceInstancesWithVersionAsync(string name, string version);
        /// <summary>
        /// 查找服务
        /// </summary>
        /// <param name="nameTagsPredicate"></param>
        /// <param name="registryInformationPredicate"></param>
        /// <returns></returns>
        Task<IList<RegistryInformation>> FindServiceInstancesAsync(Predicate<KeyValuePair<string, string[]>> nameTagsPredicate,Predicate<RegistryInformation> registryInformationPredicate);
        Task<IList<RegistryInformation>> FindServiceInstancesAsync(Predicate<KeyValuePair<string, string[]>> predicate);
        Task<IList<RegistryInformation>> FindServiceInstancesAsync(Predicate<RegistryInformation> predicate);
        /// <summary>
        /// 查所有的服务
        /// </summary>
        /// <returns></returns>
        Task<IList<RegistryInformation>> FindAllServicesAsync();
    }
}
