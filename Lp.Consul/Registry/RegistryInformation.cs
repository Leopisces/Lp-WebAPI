﻿using System;
using System.Collections.Generic;

namespace Lp.Consul.Registry
{
    /// <summary>
    /// 注册信息
    /// </summary>
    public class RegistryInformation
    {
        /// <summary>
        /// 服务名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 端口
        /// </summary>
        public int Port { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// 标签
        /// </summary>
        public IEnumerable<string> Tags { get; set; }

        /// <summary>
        /// url
        /// </summary>
        /// <param name="scheme"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public Uri ToUri(string scheme = "http", string path = "/")
        {
            var builder = new UriBuilder(scheme, Address, Port, path);
            return builder.Uri;
        }
        /// <summary>
        /// 重写tostring
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{Address}:{Port}";
        }
    }
}
