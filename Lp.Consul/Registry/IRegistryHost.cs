﻿namespace Lp.Consul.Registry
{
    public interface IRegistryHost : IManageServiceInstances,IManageHealthChecks,IResolveServiceInstances
    {
    }
}
