﻿using System;
using System.Net;

namespace Lp.Consul
{
    /// <summary>
    /// consul注册配置类
    /// </summary>
    public class ConsulRegistryHostConfiguration
    {
        public string HttpEndpoint { get; set; }

        public DnsEndpoint DnsEndpoint { get; set; }

        public string Datacenter { get; set; }

        public string AclToken { get; set; }

        public TimeSpan? LongPollMaxWait { get; set; }

        public TimeSpan? RetryDelay { get; set; } = Defaults.ErrorRetryInterval;
    }
    /// <summary>
    /// DNS端口配置类
    /// </summary>
    public class DnsEndpoint
    {
        public string Address { get; set; }
        public int Port { get; set; }
        public IPEndPoint ToIPEndPoint()
        {
            return new IPEndPoint(IPAddress.Parse(Address), Port);
        }
    }
    /// <summary>
    /// 默认类
    /// </summary>
    public static class Defaults
    {
        public static TimeSpan ErrorRetryInterval => TimeSpan.FromSeconds(15);
        public static TimeSpan UpdateMaxInterval => TimeSpan.FromSeconds(15);
    }
}
