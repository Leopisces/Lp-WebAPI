﻿using Consul;
using DnsClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using Lp.Consul.Registry;
using Microsoft.Extensions.Hosting;

namespace Lp.Consul
{
    /// <summary>
    /// 各种服务注入方法
    /// </summary>
    public static class ConsulExtension
    {
        /// <summary>
        /// 注入consul服务
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddConsulServer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions();
            services.Configure<ConsulServiceDiscoveryOption>(configuration.GetSection("ServiceDiscovery"));

            ConsulServiceDiscoveryOption serviceDiscoveryOption = new ConsulServiceDiscoveryOption();
            configuration.GetSection("ServiceDiscovery").Bind(serviceDiscoveryOption);
            ///注入consul客户端服务
            services.RegisterConsulClient();
            ///注入dns查找服务
            services.RegisterDnsLookup();
            ///注入consul服务的注册
            var serviceRegistry = new ServiceRegistry(new ConsulRegistryHost(serviceDiscoveryOption.Consul));
            services.AddSingleton(serviceRegistry);
            return services;
        }

        /// <summary>
        /// 注入DNS查找服务
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection RegisterDnsLookup(this IServiceCollection services)
        {
            //implement the dns lookup and register to service container
            services.TryAddSingleton<IDnsQuery>(p =>
            {
                var serviceConfiguration = p.GetRequiredService<IOptions<ConsulServiceDiscoveryOption>>().Value;
                var client = new LookupClient(serviceConfiguration.Consul.DnsEndpoint.ToIPEndPoint());
                return client;
            });
            return services;
        }

        /// <summary>
        /// /注册consul客户端服务
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        private static IServiceCollection RegisterConsulClient(this IServiceCollection services)
        {
            services.TryAddSingleton<IConsulClient>(p => new ConsulClient(config =>
            {
                var serviceConfiguration = p.GetRequiredService<IOptions<ConsulServiceDiscoveryOption>>().Value;
                if (!string.IsNullOrEmpty(serviceConfiguration.Consul.HttpEndpoint))
                {
                    config.Address = new Uri(serviceConfiguration.Consul.HttpEndpoint);
                }
            }));
            return services;
        }


        /// <summary>
        /// 通过服务地址生成consul服务代理
        /// 当应用程序停止时，这些服务代理将注销
        /// </summary>
        /// <param name="app"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseConsulRegisterService(this IApplicationBuilder app, IConfiguration configuration)
        {
            ConsulServiceDiscoveryOption serviceDiscoveryOption = new ConsulServiceDiscoveryOption();
            configuration.GetSection("ServiceDiscovery").Bind(serviceDiscoveryOption);
            app.UseConsulRegisterService(serviceDiscoveryOption, configuration);
            return app;
        }

        //通过服务地址生成consul服务代理
        //当应用程序停止时，这些服务代理将注销
        public static IApplicationBuilder UseConsulRegisterService(this IApplicationBuilder app, ConsulServiceDiscoveryOption serviceDiscoveryOption, IConfiguration configuration)
        {
            var applicationLifetime = app.ApplicationServices.GetRequiredService<IHostApplicationLifetime>() ??
               throw new ArgumentException("Missing Dependency", nameof(IHostApplicationLifetime));
            if (serviceDiscoveryOption.Consul == null)
                throw new ArgumentException("Missing Dependency", nameof(serviceDiscoveryOption.Consul));
            var consul = app.ApplicationServices.GetRequiredService<IConsulClient>() ?? throw new ArgumentException("Missing dependency", nameof(IConsulClient));

            //create logger to record the important information
            var loggerFactory = app.ApplicationServices.GetRequiredService<ILoggerFactory>();
            var logger = loggerFactory.CreateLogger("MisServiceBuilder");

            if (string.IsNullOrEmpty(serviceDiscoveryOption.ServiceName))
                throw new ArgumentException("service name must be configure", nameof(serviceDiscoveryOption.ServiceName));
            IEnumerable<Uri> addresses = null;
            if (serviceDiscoveryOption.Endpoints != null && serviceDiscoveryOption.Endpoints.Length > 0)
            {
                logger.LogInformation($"Using {serviceDiscoveryOption.Endpoints.Length} configured endpoints for service registration");
                addresses = serviceDiscoveryOption.Endpoints.Select(p => new Uri(p));
            }
            else
            {
                logger.LogInformation($"Trying to use server.Features to figure out the service endpoint for registration.");
                var features = app.Properties["server.Features"] as FeatureCollection;
                addresses = features.Get<IServerAddressesFeature>().Addresses.Select(p => new Uri(p)).ToArray();
                logger.LogInformation(@"addresses:" + addresses.FirstOrDefault() + ",count:" + addresses.Count());
            }
            foreach (var address in addresses)
            {
                var serviceID = GetServiceId(serviceDiscoveryOption.ServiceName, address);
                logger.LogInformation($"Registering service {serviceID} for address {address}.");
                Uri healthCheck = null;
                if (!string.IsNullOrEmpty(serviceDiscoveryOption.HealthCheckTemplate))
                {
                    healthCheck = new Uri(address, serviceDiscoveryOption.HealthCheckTemplate);
                    logger.LogInformation($"Adding healthcheck for {serviceID},checking {healthCheck}");
                }
                var registryInformation = app.AddTenant(serviceDiscoveryOption.ServiceName, serviceDiscoveryOption.Version, address, healthCheckUri: healthCheck, tags: new[] { $"urlprefix-/{serviceDiscoveryOption.ServiceName}" });
                logger.LogInformation("Registering additional health check");
                // register service & health check cleanup
                applicationLifetime.ApplicationStopping.Register(() =>
                {
                    logger.LogInformation("Removing tenant & additional health check");
                    app.RemoveTenant(registryInformation.Id);
                });
            }
            return app;
        }

        /// <summary>
        /// 获取服务编码
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="uri"></param>
        /// <returns></returns>
        private static string GetServiceId(string serviceName, Uri uri)
        {
            return $"WebAPI_{serviceName}_{uri.Host.Replace(".", "_")}_{uri.Port}";
        }

        /// <summary>
        /// 添加服务租户
        /// </summary>
        /// <param name="app"></param>
        /// <param name="serviceName"></param>
        /// <param name="version"></param>
        /// <param name="uri"></param>
        /// <param name="healthCheckUri"></param>
        /// <param name="tags"></param>
        /// <returns></returns>
        public static RegistryInformation AddTenant(this IApplicationBuilder app, string serviceName, string version, Uri uri, Uri healthCheckUri = null, IEnumerable<string> tags = null)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            var serviceRegistry = app.ApplicationServices.GetRequiredService<ServiceRegistry>();
            var registryInformation = serviceRegistry.RegisterServiceAsync(serviceName, version, uri, healthCheckUri, tags)
                .Result;

            return registryInformation;
        }

        /// <summary>
        /// 移除服务租户
        /// </summary>
        /// <param name="app"></param>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        public static bool RemoveTenant(this IApplicationBuilder app, string serviceId)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }
            if (string.IsNullOrEmpty(serviceId))
            {
                throw new ArgumentNullException(nameof(serviceId));
            }

            var serviceRegistry = app.ApplicationServices.GetRequiredService<ServiceRegistry>();
            return serviceRegistry.DeregisterServiceAsync(serviceId)
                .Result;
        }

        /// <summary>
        /// 添加健康监控
        /// </summary>
        /// <param name="app"></param>
        /// <param name="registryInformation"></param>
        /// <param name="checkUri"></param>
        /// <param name="interval"></param>
        /// <param name="notes"></param>
        /// <returns></returns>
        public static string AddHealthCheck(this IApplicationBuilder app, RegistryInformation registryInformation, Uri checkUri, TimeSpan? interval = null, string notes = null)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }
            if (registryInformation == null)
            {
                throw new ArgumentNullException(nameof(registryInformation));
            }

            var serviceRegistry = app.ApplicationServices.GetRequiredService<ServiceRegistry>();
            string checkId = serviceRegistry.AddHealthCheckAsync(registryInformation.Name, registryInformation.Id, checkUri, interval, notes)
                .Result;

            return checkId;
        }

        /// <summary>
        /// 移除健康监控
        /// </summary>
        /// <param name="app"></param>
        /// <param name="checkId"></param>
        /// <returns></returns>
        public static bool RemoveHealthCheck(this IApplicationBuilder app, string checkId)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }
            if (string.IsNullOrEmpty(checkId))
            {
                throw new ArgumentNullException(nameof(checkId));
            }

            var serviceRegistry = app.ApplicationServices.GetRequiredService<ServiceRegistry>();
            return serviceRegistry.DeregisterHealthCheckAsync(checkId)
                .Result;
        }
    }
}
