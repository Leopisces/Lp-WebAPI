using Lp.Core;
using Lp.Core.Extensions;
using Lp.JWT;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ocelot.Cache.CacheManager;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Consul;
using Ocelot.Provider.Polly;
using Swashbuckle.AspNetCore.SwaggerUI;
using System.Collections.Generic;
using Lp.Core.Entity;
using Lp.ServiceExtension;
using Lp.Consul;
using Lp.Swagger;

namespace OcelotGateway
{
    /// <summary>
    /// 配置
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// 配置
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// 所有的API信息列表
        /// </summary>
        private List<string> list = new List<string>();

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            //注入API信息
            services.AddSingleton(ApiInfo.Instantiate(Configuration, GetType().Assembly));
            list = Configuration.GetSection("ApiList").Get<List<string>>();

            //注入OcelotJwtBearer
            var audienceConfig = Configuration.GetSection("Audience");
            services.AddOcelotJwtBearer(audienceConfig["Issuer"], audienceConfig["Issuer"], audienceConfig["Secret"], "GSWBearer");
            services.AddOcelot().AddCacheManager(x => { x.WithDictionaryHandle(); }).AddConsul().AddConfigStoredInConsul().AddPolly();

            //注入Consul
            services.AddConsulServer(Configuration);

            //注入swagger
            if (Configuration.GetSection("UseSwagger") != null && Configuration.GetSection("UseSwagger").Value == "true")
            {
                services.AddCustomSwagger(ApiInfo.Instance);
            }
            //日志
            //services.AddMisServerLog(Configuration, GetType().Assembly.GetName());
            //注入跨域
            services.AddPermissiveCors();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.EnvironmentName == "Development")
            {
                app.UseDeveloperExceptionPage();
            }
            //使用通用日志类
            //app.UseMisLogService();
            //使用consul注册服务
            app.UseConsulRegisterService(Configuration);
            //使用swagger
            app.UseRouting();
            //权限
            app.UseAuthorization();
            //跨域
            app.UsePermissiveCors();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            if (Configuration.GetSection("UseSwagger") != null && Configuration.GetSection("UseSwagger").Value == "true")
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    list.ForEach(m =>
                    {
                        c.SwaggerEndpoint($"/{m}/swagger.json", m);
                        //SWAGGER UI折叠
                        c.DocExpansion(DocExpansion.None);
                    });
                });
            }
            //使用Ocelot中间件
            app.UseOcelot().Wait();
        }
    }

}
