﻿
using Microsoft.AspNetCore.Mvc;

namespace OcelotGateway
{
    /// <summary>
    /// 网关健康检查
    /// </summary>
    [Produces("application/json")]
    [Route("api/OcelotGateway/Health")]
    public class HealthController : Controller
    {
        /// <summary>
        /// OK
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get() => Ok("ok");
    }
}