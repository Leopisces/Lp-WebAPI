using Com.Ctrip.Framework.Apollo;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;
using Ocelot.DependencyInjection;
using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace OcelotGateway
{
    /// <summary>
    /// 程序入口
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// CreateWebHostBuilder
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IHostBuilder CreateWebHostBuilder(string[] args) =>
               Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, builder) =>
                {
                    builder.AddApollo(builder.Build().GetSection("Apollo"))
                           .AddDefault()
                           .AddNamespace("GeneralConfiguration.json")
                           .AddNamespace("OcelotGateway.json")
                           .AddJsonFile("ocelot.json")
                           .AddEnvironmentVariables();
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.ClearProviders();//移除其它已经注册的日志处理程序
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                    logging.AddDebug();
                })
                 .ConfigureWebHostDefaults(webBuilder =>
                 {
                     webBuilder
                     .UseStartup<Startup>()
                     .UseNLog()
                     .ConfigureKestrel((Context, options) =>
                     {
                         options.Limits.KeepAliveTimeout = TimeSpan.FromMilliseconds(800);
                         options.AllowSynchronousIO = true;

                         options.Listen(IPAddress.Any, int.Parse(Context.Configuration.GetSection("Ports:http").Value), listenOptions =>
                         {
                             listenOptions.Protocols = HttpProtocols.Http1AndHttp2;
                             listenOptions.UseConnectionLogging();
                         });
                     });
                 });
    }
}
