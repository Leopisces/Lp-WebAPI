﻿using System;
namespace Lp.Redis
{
    public interface IRedisContext
    {

        //获取 Reids 缓存值
        string GetValue(string key, int index = -1);

        //获取值，并序列化
        TEntity Get<TEntity>(string key, int index = -1);

        //保存
        void Set(string key, object value, TimeSpan cacheTime, int index = -1);

        //判断是否存在
        bool Get(string key, int index = -1);

        //移除某一个缓存值
        void Remove(string key, int index = -1);

        //全部清除
        void Clear(int index = -1);

        /// <summary>
        /// 设置Hash值
        /// </summary>
        bool SetHash(string key, object hashKey, object hashValue, int index = -1);

        /// <summary>
        /// 设置Hash值
        /// </summary>
        bool SetHashCheckExists(string key, object hashKey, object hashValue, int index = -1);

        /// <summary>
        /// 清除Hash值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="hashKey"></param>
        /// <param name="hashValue"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        bool HashDelete(string key, object hashKey, int index = -1);

        /// <summary>
        /// 获取Hash值
        /// </summary>
        T GetHash<T>(string key, object hashKey, int index = -1);
    }
}
