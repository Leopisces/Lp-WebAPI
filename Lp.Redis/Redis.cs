﻿using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Collections.Generic;

namespace Lp.Redis
{
    public class RedisContext : IRedisContext
    {

        private readonly RedisOption _RedisOption;
        private ConnectionMultiplexer redisConnection;
        private object redisConnectionLock = new object();
        public RedisContext(IOptions<RedisOption> options)
        {
            this._RedisOption = options.Value;
            this.redisConnection = GetRedisConnection();
        }




        /// <summary>
        /// 核心代码，获取连接实例
        /// 通过双if 夹lock的方式，实现单例模式
        /// </summary>
        /// <returns></returns>
        private ConnectionMultiplexer GetRedisConnection()
        {
            //如果已经连接实例，直接返回
            if (this.redisConnection != null && this.redisConnection.IsConnected)
            {
                return this.redisConnection;
            }
            //加锁，防止异步编程中，出现单例无效的问题
            lock (redisConnectionLock)
            {
                if (this.redisConnection != null)
                {
                    //释放redis连接
                    this.redisConnection.Dispose();
                }
                try
                {
                    var config = new ConfigurationOptions
                    {
                        AbortOnConnectFail = false,
                        AllowAdmin = true,
                        ConnectTimeout = 15000,
                        SyncTimeout = 5000,
                        EndPoints = { _RedisOption.ConnectionString },
                        Password = _RedisOption.Password
                    };

                    //this.redisConnection = ConnectionMultiplexer.Connect(_RedisOption.ConnectionString);
                    this.redisConnection = ConnectionMultiplexer.Connect(config);

                }
                catch (Exception)
                {

                    throw new Exception("Redis服务未启用，请开启该服务");
                }
            }
            return this.redisConnection;
        }
        /// <summary>
        /// 清除
        /// </summary>
        public void Clear(int index = -1)
        {
            foreach (var endPoint in this.GetRedisConnection().GetEndPoints())
            {
                var server = this.GetRedisConnection().GetServer(endPoint);
                foreach (var key in server.Keys())
                {
                    redisConnection.GetDatabase(index).KeyDelete(key);
                }
            }
        }
        /// <summary>
        /// 判断是否存在
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Get(string key, int index = -1)
        {
            return redisConnection.GetDatabase(index).KeyExists(key);
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetValue(string key, int index = -1)
        {

            return redisConnection.GetDatabase(index).StringGet(key);
        }

        /// <summary>
        /// 获取
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public TEntity Get<TEntity>(string key, int index = -1)
        {
            var value = redisConnection.GetDatabase(index).StringGet(key);
            if (value.HasValue)
            {
                //需要用的反序列化，将Redis存储的Byte[]，进行反序列化
                return SerializeHelper.Deserialize<TEntity>(value);
            }
            else
            {
                return default(TEntity);
            }
        }

        /// <summary>
        /// 移除
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key, int index = -1)
        {
            redisConnection.GetDatabase(index).KeyDelete(key);
        }
        /// <summary>
        /// 设置
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="cacheTime"></param>
        public void Set(string key, object value, TimeSpan cacheTime, int index = -1)
        {
            if (value != null)
            {
                try
                {
                    //序列化，将object值生成RedisValue
                    redisConnection.GetDatabase(index).StringSet(key, SerializeHelper.Serialize(value), cacheTime);

                    //redisConnection.GetDatabase(index).SortedSetAdd()
                }
                catch //(Exception ex)
                {
                    //TODU高并发会中断连接 
                    throw new Exception("Redis连接失败");
                }

            }
        }

        /// <summary>
        /// 增加/修改
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool SetValue(string key, byte[] value, int index = -1)
        {
            return redisConnection.GetDatabase(index).StringSet(key, value, TimeSpan.FromSeconds(1200));
        }

        public bool SortedSetAdd(string key, string value, double score, int index = -1)
        {
            return redisConnection.GetDatabase(index).SortedSetAdd(key, value, score);
        }
        public List<T> SortedSetRangSorted<T>(string key, int take, int skip, int index = -1)
        {
            var r = redisConnection.GetDatabase(index)
                .SortedSetRangeByScore(key, 0, int.MaxValue, Exclude.None, Order.Ascending, skip, take, CommandFlags.None);
            var list = new List<T>();
            foreach (var node in r)
            {
                list.Add(SerializeHelper.Deserialize<T>(node));
            }
            return list;

        }


        public bool SetHash(string key, object hashKey, object hashValue, int index = -1)
        {
            var r = redisConnection.GetDatabase(index).HashSet(key, SerializeHelper.Serialize(hashKey), SerializeHelper.Serialize(hashValue));
            return r;
        }

        public bool SetHashCheckExists(string key, object hashKey, object hashValue, int index = -1)
        {
            var r = false;
            if (!redisConnection.GetDatabase(index).HashExists(key, SerializeHelper.Serialize(hashKey)))
            {
                r = redisConnection.GetDatabase(index).HashSet(key, SerializeHelper.Serialize(hashKey), SerializeHelper.Serialize(hashValue));
            }
            return r;
        }
        public T GetHash<T>(string key, object hashKey, int index = -1)
        {
            var r = redisConnection.GetDatabase(index).HashGet(key, SerializeHelper.Serialize(hashKey));
            var obj = SerializeHelper.Deserialize<T>(r);
            return obj;
        }

        public bool HashDelete(string key, object hashKey, int index = -1)
        {
            var r = false;
            if (redisConnection.GetDatabase(index).HashExists(key, SerializeHelper.Serialize(hashKey)))
            {
                r = redisConnection.GetDatabase(index).HashDelete(key, SerializeHelper.Serialize(hashKey));
            }
            return r;
        }
    }

}
