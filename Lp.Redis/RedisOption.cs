﻿namespace Lp.Redis
{
    public class RedisOption
    {
        public bool Enable { get; set; }
        public string ConnectionString { get; set; }
        public string Password { get; set; }

    }
}
