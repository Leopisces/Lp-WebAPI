﻿namespace Lp.Redis
{
    public partial class RedisConfigure
    {
        /// <summary>
        /// 登录
        /// </summary>
        public static RedisConfigureNode LOGIN = new RedisConfigureNode { RedisIndex = 0, RedisKey = "UserToken:{0}:{1}", CatchTimeMinite = 0 };

        /// <summary>
        /// 字段权限Field:{0}:{1} UserId,Path
        /// </summary>
        public static RedisConfigureNode FIELD = new RedisConfigureNode { RedisIndex = 2, RedisKey = "Field:{0}:{1}", CatchTimeMinite = 5 };


    }
}
