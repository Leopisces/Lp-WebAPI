﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using Lp.Core.Entity;
using Lp.LogAnalysis;
using Microsoft.AspNetCore.Http;
using Lp.JWT;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.HttpOverrides;
using Newtonsoft.Json.Serialization;
using System.Text.Unicode;
using Lp.Redis;
using Lp.SqlSugarORM;
using Lp.Core.Attributes;
using Microsoft.AspNetCore.Hosting;
using Lp.Grpc;
using Lp.Consul;
using Lp.Swagger;

namespace Lp.ServiceExtension
{
    /// <summary>
    /// 各种服务注入方法
    /// </summary>
    public static class ServiceCollectionCollection
    {
        /// <summary>
        /// 使用通用的GRPC管道服务
        /// </summary>
        /// <param name="app"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseCommonGrpcService(this IApplicationBuilder app, IConfiguration Configuration)
        {
            //使用通用日志类
            //app.UseMisLogService();
            //使用Grpc服务
            app.UseGrpcConsulRegisterService(Configuration);
            app.UseRouting();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            return app;
        }

        /// <summary>
        /// 使用通用的IS服务
        /// </summary>
        /// <param name="app"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseCommonService(this IApplicationBuilder app, IWebHostEnvironment env, IConfiguration Configuration, IApiInfo apiInfo)
        {
            //使用通用日志类
            //app.UseMisLogService();
            //使用consul注册服务
            app.UseConsulRegisterService(Configuration);
            app.UseRouting();
            //授权
            app.UseAuthorization();
            //设置获取客户端ip
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            //swagger
            app.UseCustomSwagger(apiInfo);
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            return app;
        }

        /// <summary>
        /// 通用的GRPC注册服务
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddCommonGrpcServer(this IServiceCollection services, IConfiguration Configuration, Assembly assembly)
        {
            services.AddControllers();
            Console.WriteLine("Redis:ConnectionString:" + Configuration.GetSection("Redis:ConnectionString").Value);

            //注入Consul服务
            services.AddConsulServer(Configuration);
            //注入API信息
            services.AddSingleton(ApiInfo.Instantiate(Configuration, assembly));
            //注入日志
            //services.AddMisServerLog(Configuration, assembly.GetName());
            //注入redis文件配置注入
            if (!string.IsNullOrEmpty(Configuration.GetSection("Redis:ConnectionString").Value))
            {
                services.AddSingleton<IRedisContext, RedisContext>();
                services.Configure<RedisOption>(Configuration.GetSection("Redis"));
            }
            //end
            //注入Sugar数据库注入,因为是数组,所以不能加判断,要不无法获取配置
            services.Configure<SugarOption>(Configuration.GetSection("Sugar"));

            //注入GrpcClient
            services.AddGrpcClient();
            return services;
        }

        /// <summary>
        /// 通用的webapi注册服务
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddCommonServer(this IServiceCollection services, IConfiguration Configuration, Assembly assembly)
        {
            //注入日志
            //services.AddMisServerLog(Configuration, assembly.GetName());
            //注入controller
            services.AddControllers();
            //注入API信息
            services.AddSingleton(ApiInfo.Instantiate(Configuration, assembly));
            //注入swagger
            services.AddCustomSwagger(ApiInfo.Instance);
            //注入请求的上下文
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            //注入Consul服务
            services.AddConsulServer(Configuration);
            services.AddGrpcClient();
            //end
            //注入分布式内存缓存
            services.AddDistributedMemoryCache();
            //注入redis文件配置注入
            if (!string.IsNullOrEmpty(Configuration.GetSection("Redis:ConnectionString").Value))
            {
                services.AddSingleton<IRedisContext, RedisContext>();
                services.Configure<RedisOption>(Configuration.GetSection("Redis"));
            }
            //注入Sugar数据库注入,因为是数组,所以不能加判断,要不无法获取配置
            services.Configure<SugarOption>(Configuration.GetSection("Sugar"));
            //注入JWT
            var audienceConfig = Configuration.GetSection("Audience");
            int ValidateTime = 2;
            if (audienceConfig != null && !string.IsNullOrEmpty(audienceConfig["ValidateTime"].ToString()))
            {
                ValidateTime = int.Parse(audienceConfig["ValidateTime"].ToString());
            }
            //应用API的Token
            if (audienceConfig["Identification"] == "Producer")
            {
                services.AddJTokenBuild(ValidateTime, audienceConfig["Issuer"], audienceConfig["Issuer"], audienceConfig["Secret"], "/api/denied");
            }
            else if (audienceConfig["Identification"] == "Use")
            {
                services.AddOcelotPolicyJwtBearer(ValidateTime, audienceConfig["Issuer"], audienceConfig["Issuer"], audienceConfig["Secret"], "GSWBearer", "Permission", "/api/Denied");
                //这个集合模拟用户权限表,可从数据库中查询出来
                var permission = new List<Permission> {
                              //new Permission {  Url="/DemoBAPI/values", Name="admin"},
                              new Permission {  Url="/", Name="admin"}
                          };
                services.AddSingleton(permission);
            }
            //自动驼峰格式化问题-han
            //注入全局异常捕获
            services.AddMvc(option =>
            {
                option.Filters.Add(typeof(ApiExceptionFilterAttribute));
            });
            services.AddMvc()
            //.AddMvcApiResult()
            .AddJsonOptions(options =>
            {

                options.JsonSerializerOptions.Encoder = JavaScriptEncoder.Create(UnicodeRanges.All);
                options.JsonSerializerOptions.PropertyNamingPolicy = null;

            })
           .AddNewtonsoftJson(options =>
           {
               options.SerializerSettings.ContractResolver = new DefaultContractResolver();
               //han:10-20
               //options.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Unspecified;
               //options.SerializerSettings.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat;
               options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";

           });
            //获取客户端ip
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                //阿里k8s部署要加这个
                options.ForwardLimit = 2;
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });
            return services;
        }

        /// <summary>
        /// 注入跨域服务
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddPermissiveCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("PermissiveCorsPolicy", builder => builder
                   .AllowAnyHeader()
                   .AllowAnyMethod()
                   .AllowAnyOrigin()
                  );
            });
            return services;
        }

        /// <summary>
        /// 添加跨域
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UsePermissiveCors(this IApplicationBuilder app) => app.UseCors("PermissiveCorsPolicy");
    }
}
