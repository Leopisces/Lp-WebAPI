﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Lp.ServiceExtension
{
    public class CommonMethod
    {
        public static Dictionary<Type, Type> InjectClassTypes(string className, string prefix, string endwith)
        {
            if (!string.IsNullOrEmpty(className))
            {
                List<Type> ts = Assembly.GetEntryAssembly().GetTypes().ToList();
                var result = new Dictionary<Type, Type>();
                foreach (var item in ts.Where(s => !s.IsInterface))
                {
                    if (item.Name == className + "`")
                    {
                        continue;
                    }
                    if (!item.Name.ToUpper().EndsWith(endwith.ToUpper()))
                    {
                        continue;
                    }
                    if (!item.FullName.ToUpper().StartsWith(className.ToUpper()))
                    {
                        continue;
                    }
                    string itemName = prefix + item.Name;
                    var interfaceTypes = item.GetInterfaces();
                    bool isResistory = true;
                    var interfaceType = interfaceTypes.Where(c => c.Name == itemName).FirstOrDefault();
                    if (interfaceType != null && isResistory)
                    {
                        result.Add(item, interfaceType);
                    }
                }
                return result;
            }
            return new Dictionary<Type, Type>();
        }
    }
}
